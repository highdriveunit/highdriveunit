/*
 * HDU_BATSENSE.c
 *
 *  Created on: Aug 8, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_BATSENSE.h>

HDU_ADC0_Data vbat_adc; // SE4b

float ratio;

void HDU_BATSENSE_init(int r1, int r2)
{
	HDU_ADC0_init();

	vbat_adc.channel = 4;
	vbat_adc.mux = kADC16_ChannelMuxB;

	ratio = (3.3 / 4096.0) * (1 / ((float)r2 / (float)(r1 + r2)));
}

float HDU_BATSENSE_read()
{
	return ratio * (float)HDU_ADC0_read(&vbat_adc);
}
