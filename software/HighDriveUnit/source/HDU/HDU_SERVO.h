/*
 * HDU_SERVO.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for servo control
 */

#ifndef HDU_HDU_SERVO_H_
#define HDU_HDU_SERVO_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_tpm.h"

/*!
 * @brief Initializes servos on TPM1 channel 0 and channel 1
 * @param frequencyHz of TPM1 ( 50 Hz for standard servo types )
 */
void HDU_SERVO_init(uint32_t frequencyHz);

/*!
 * @brief sets PWM dutycycle for servo 0 with safety check ( 5% - 10% )
 * @param dutycycle as decimal
 */
void HDU_SERVO_0_setDutycycle(float dutycycle);

/*!
 * @brief sets PWM dutycycle for servo 1 with safety check ( 5% - 10% )
 * @param dutycycle as decimal from 0 to 1
 */
void HDU_SERVO_1_setDutycycle(float dutycycle);

/*!
 * @brief sets steering angle of servo 0
 * @param angle from -90 degrees to 90 degrees
 */
void HDU_SERVO_0_setSteeringAngle(int angle);

/*!
 * @brief sets steering angle of servo 1
 * @param angle from -90 degrees to 90 degrees
 */
void HDU_SERVO_1_setSteeringAngle(int angle);

#endif /* HDU_HDU_SERVO_H_ */
