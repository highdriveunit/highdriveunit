/*
 * HDU_SERVO.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_SERVO.h>

tpm_config_t tpm1Conf;

tpm_chnl_pwm_signal_param_t servo_0_pwm;
tpm_chnl_pwm_signal_param_t servo_1_pwm;

void HDU_SERVO_init(uint32_t frequencyHz)
{
	servo_0_pwm.chnlNumber = (tpm_chnl_t)0U;
	servo_0_pwm.level = kTPM_HighTrue;
	servo_0_pwm.dutyCyclePercent = 7;

	servo_1_pwm.chnlNumber = (tpm_chnl_t)1U;
	servo_1_pwm.level = kTPM_HighTrue;
	servo_1_pwm.dutyCyclePercent = 7;

	tpm_chnl_pwm_signal_param_t servoPWMConf[2];
	servoPWMConf[0] = servo_0_pwm;
	servoPWMConf[1] = servo_1_pwm;

	CLOCK_SetTpmClock(1U);
	TPM_GetDefaultConfig(&tpm1Conf);
	tpm1Conf.prescale = kTPM_Prescale_Divide_16;
	TPM_Init(TPM1, &tpm1Conf);
    TPM_SetupPwm(TPM1, servoPWMConf, 2U, kTPM_CenterAlignedPwm, frequencyHz, CLOCK_GetFreq(kCLOCK_PllFllSelClk));
    TPM_StartTimer(TPM1, kTPM_SystemClock);
}

void HDU_SERVO_0_setDutycycle(float dutycycle)
{
	if(dutycycle > 0.1)
	{
		dutycycle = 0.1;
	}
	else if(dutycycle < 0.05)
	{
		dutycycle = 0.05;
	}
	TPM1->CONTROLS[0].CnV = dutycycle * TPM1->MOD;
}

void HDU_SERVO_1_setDutycycle(float dutycycle)
{
	if(dutycycle > 0.1)
	{
		dutycycle = 0.1;
	}
	else if(dutycycle < 0.05)
	{
		dutycycle = 0.05;
	}
	TPM1->CONTROLS[1].CnV = dutycycle * TPM1->MOD;
}

void HDU_SERVO_0_setSteeringAngle(int angle)
{
	angle += 90;
	HDU_SERVO_0_setDutycycle((0.05 + (float)angle * (0.05 / 180.0)));
}

void HDU_SERVO_1_setSteeringAngle(int angle)
{
	angle += 90;
	HDU_SERVO_1_setDutycycle((0.05 + (float)angle * (0.05 / 180.0)));
}
