/*
 * HDU_LINESCAN.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_LINESCAN.h>

HDU_LINESCAN_Data HDU_LINESCAN_data;

HDU_ADC0_Data cam_0_adc; // SE6b
HDU_ADC0_Data cam_1_adc; // SE7b

pit_config_t pitConfig;

void HDU_LINESCAN_init(uint64_t periodUs)
{
//	HDU_ADC0_init();

	cam_0_adc.channel = 6;
	cam_0_adc.mux = kADC16_ChannelMuxB;
	cam_1_adc.channel = 7;
	cam_1_adc.mux = kADC16_ChannelMuxB;

	PIT_GetDefaultConfig(&pitConfig);
    PIT_Init(PIT, &pitConfig);
    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(periodUs, CLOCK_GetFreq(kCLOCK_BusClk)));
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
    EnableIRQ(PIT_IRQn);
    PIT_StartTimer(PIT, kPIT_Chnl_0);

    HDU_LINESCAN_data.available = false;
    HDU_LINESCAN_data.cam0En = false;
    HDU_LINESCAN_data.cam1En = false;
}

void HDU_LINESCAN_enable0()
{
	HDU_LINESCAN_data.cam0En = true;
}

void HDU_LINESCAN_enable1()
{
	HDU_LINESCAN_data.cam1En = true;
}

void HDU_LINESCAN_disable0()
{
	HDU_LINESCAN_data.cam0En = false;
}

void HDU_LINESCAN_disable1()
{
	HDU_LINESCAN_data.cam0En = false;
}

HDU_LINESCAN_Data HDU_LINESCAN_capture()
{
	while(!HDU_LINESCAN_data.available){}
	HDU_LINESCAN_Data data;
	data.available=true;

	data.cam0En = HDU_LINESCAN_data.cam0En;
	data.cam1En = HDU_LINESCAN_data.cam1En;

	if(data.cam0En)
	{
		memcpy(data.pixels0, HDU_LINESCAN_data.pixels0, 128);
	}
	if(data.cam1En)
	{
		memcpy(data.pixels1, HDU_LINESCAN_data.pixels1, 128);
	}

	return data;
}

void HDU_LINESCAN_read()
{
	GPIO_WritePinOutput(HDU_CAM_SYNC_GPIO, HDU_CAM_SYNC_PIN, 1);
	asm("nop");
	GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 1);
	asm("nop");
	GPIO_WritePinOutput(HDU_CAM_SYNC_GPIO, HDU_CAM_SYNC_PIN, 0);
	GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 0);

	HDU_LINESCAN_data.available = false;


	for(int i = 0; i < 128; i++)
	{
		asm("nop");
		asm("nop");
		GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 1);
		if(HDU_LINESCAN_data.cam0En)
		{
			HDU_LINESCAN_data.pixels0[i] = HDU_ADC0_read(&cam_0_adc);
		}
		if(HDU_LINESCAN_data.cam0En)
		{
			HDU_LINESCAN_data.pixels1[i] = HDU_ADC0_read(&cam_1_adc);
		}
		GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 0);
	}

	HDU_LINESCAN_data.available = true;
	asm("nop");
	asm("nop");
	GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 1);
	asm("nop");
	asm("nop");
	GPIO_WritePinOutput(HDU_CAM_CLK_GPIO, HDU_CAM_CLK_PIN, 0);
}

void PIT_IRQHandler()
{
    PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);

    HDU_LINESCAN_read();
}
