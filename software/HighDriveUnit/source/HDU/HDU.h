/*
 * HDU.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      HDU ( High Drive Unit ) library core fusionating all available modules
 *      ADC0, BATSENSE, BLUETOOTH, HMI, LINESCAN, POWERTRAIN, SERVO
 */

#ifndef HDU_HDU_H_
#define HDU_HDU_H_

#include <HDU/HDU_ADC0.h>
#include <HDU/HDU_BATSENSE.h>
#include <HDU/HDU_BLUETOOTH.h>
#include <HDU/HDU_HMI.h>
#include <HDU/HDU_LINESCAN.h>
#include <HDU/HDU_POWERTRAIN.h>
#include <HDU/HDU_SERVO.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

/*!
 * @brief Sets up and initializes HDU library. Gives also bluetooth init messages.
 */
void HDU_init();

/*!
 * @brief starts up everything from pin configurator
 * !!! if a hdu module is used standalone ( no generic HDU_init() ), !!!
 * !!! this functtion needs to be called separately !!!
 */
void HDU_startup();
#endif /* HDU_HDU_H_ */
