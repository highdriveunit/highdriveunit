/*
 * HDU.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU.h>

void HDU_init()
{
	HDU_startup();

	HDU_BLUETOOTH_init(9600);
	HDU_BLUETOOTH_sendMessage("***************************\n");
	HDU_BLUETOOTH_sendMessage("HighDriveUnit (HDU)\n\n");
	printf("***************************\n");
	printf("HighDriveUnit (HDU)\n\n");

	HDU_HMI_init();
	HDU_BLUETOOTH_sendMessage("HMI initialized...\n");
	printf("HMI initialized...\n");

	HDU_POWERTRAIN_init(3000);
	HDU_BLUETOOTH_sendMessage("Powertrain initialized...\n");
	printf("Powertrain initialized...\n");

	HDU_LINESCAN_init(200000);
	HDU_BLUETOOTH_sendMessage("Cameras initialized...\n");
	printf("Cameras initialized...\n");

	HDU_SERVO_init(50);
	HDU_BLUETOOTH_sendMessage("Servos initialized...\n\n");
	printf("Servos initialized...\n\n");

	HDU_BATSENSE_init(100, 56);
	HDU_BLUETOOTH_sendDebugMesssage("VBat", HDU_BATSENSE_read(), "V");
	HDU_BLUETOOTH_sendMessage("***************************\n");
	printf("VBAT: %fV\n", HDU_BATSENSE_read());
	printf("***************************\n");
}

void HDU_startup()
{
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
    BOARD_InitDebugConsole();

    HDU_ADC0_init();
}

