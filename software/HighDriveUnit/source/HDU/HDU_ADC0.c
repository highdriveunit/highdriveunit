/*
 * ADC0.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_ADC0.h>

adc16_config_t adc0ConfigStruct;
adc16_channel_config_t adc0ChannelConfigStruct;

void HDU_ADC0_init()
{
    ADC16_GetDefaultConfig(&adc0ConfigStruct);
	#ifdef BOARD_ADC_USE_ALT_VREF
		adc0ConfigStruct.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
	#endif
		ADC16_Init(ADC0, &adc0ConfigStruct);
		ADC16_EnableHardwareTrigger(ADC0, false); /* Make sure the software trigger is used. */
	#if defined(FSL_FEATURE_ADC16_HAS_CALIBRATION) && FSL_FEATURE_ADC16_HAS_CALIBRATION
		if (kStatus_Success == ADC16_DoAutoCalibration(ADC0))
		{
			// Calibration ok
		}
		else
		{
			// Calibration not ok
		}
	#endif /* FSL_FEATURE_ADC16_HAS_CALIBRATION */

		adc0ChannelConfigStruct.enableInterruptOnConversionCompleted = false;
	//#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
		adc0ChannelConfigStruct.enableDifferentialConversion = false;
	//#endif /* FSL_FEATURE_ADC16_HAS_DIFF_MODE */
}

uint16_t HDU_ADC0_read(HDU_ADC0_Data *data)
{
	adc0ChannelConfigStruct.channelNumber = data->channel;
    ADC16_SetChannelMuxMode(ADC0, data->mux);
	ADC16_SetChannelConfig(ADC0, 0U, &adc0ChannelConfigStruct);
    while (0U == (kADC16_ChannelConversionDoneFlag & ADC16_GetChannelStatusFlags(ADC0, 0U))){}

    data->value =  ADC16_GetChannelConversionValue(ADC0, 0U);

    return data->value;
}
