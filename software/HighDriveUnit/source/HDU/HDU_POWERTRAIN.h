/*
 * HDU_POWERTRAIN.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for controlling the powertrain featureing two dc motors
 */

#ifndef HDU_HDU_POWERTRAIN_H_
#define HDU_HDU_POWERTRAIN_H_

#include <HDU/HDU_ADC0.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_tpm.h"

/*!
 * @brief struct for saving all values of a motor
 */
typedef struct HDU_POWERTRAIN_MotorValues{
	int dutyCycle;
	uint16_t currentSense, backEMF;
}HDU_POWERTRAIN_MotorValues;

extern HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotA;
extern HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotB;

/*!
 * @brief Initializes powertrain using TPM0 ( channel 0 - 4 ). Motors are enabled by default.
 * @brief frequencyHz motor pwm frequency in Hz ( about 3 kHz )
 */
void HDU_POWERTRAIN_init(uint32_t frequencyHz);

/*!
 * @brief updates all values in motor a struct
 */
void HDU_POWERTRAIN_updateMotorValuesA();

/*!
 * @brief updates all values in motor b struct
 */
void HDU_POWERTRAIN_updateMotorValuesB();

/*!
 * @brief updates all values in motor a and motor b struct
 */
void HDU_POWERTRAIN_updateMotorValues();

/*!
 * @brief enables motors
 */
void HDU_POWERTRAIN_enable();

/*!
 * @brief disables motors
 */
void HDU_POWERTRAIN_disable();

/*!
 * @brief sets dutycycle of motor a
 * @param dutyCycle from -100 to 100 ( % )
 */
void HDU_POWERTRAIN_setDutycycleMotA(int dutyCycle);

/*!
 * @brief sets dutycycle of motor b
 * @param dutyCycle from -100 to 100 ( % )
 */
void HDU_POWERTRAIN_setDutycycleMotB(int dutyCycle);

/*!
 * @brief sets dutycycle of motor a and b
 * @param dutyCycleA from -100 to 100 ( % )
 * @param dutyCycleB from -100 to 100 ( % )
 */
void HDU_POWERTRAIN_setDutycycles(int dutyCycleA, int dutyCycleB);


/*!
 * @brief reads current sense output of motor a unit
 * @retval current from 0 to fff
 */
uint16_t HDU_POWERTRAIN_readCsA();

/*!
 * @brief reads current sense output of motor b unit
 * @retval current from 0 to fff
 */
uint16_t HDU_POWERTRAIN_readCsB();

/*!
 * @brief reads back emf voltage of motor a
 * @retval bEMF from 0 to fff
 */
uint16_t HDU_POWERTRAIN_readBemfA();

/*!
 * @brief reads back emf voltage of motor b
 * @retval bEMF from 0 to fff
 */
uint16_t HDU_POWERTRAIN_readBemfB();


#endif /* HDU_HDU_POWERTRAIN_H_ */
