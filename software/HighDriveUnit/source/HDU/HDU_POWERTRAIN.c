/*
 * HDU_POWERTRAIN.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_POWERTRAIN.h>

tpm_config_t tpm0Conf;

tpm_chnl_pwm_signal_param_t mot_a_pwm1;
tpm_chnl_pwm_signal_param_t mot_a_pwm2;
tpm_chnl_pwm_signal_param_t mot_b_pwm1;
tpm_chnl_pwm_signal_param_t mot_b_pwm2;

HDU_ADC0_Data mot_a_cs_adc; // SE3
HDU_ADC0_Data mot_b_cs_adc; // SE7a
HDU_ADC0_Data mot_a_bemf_adc; // SE14
HDU_ADC0_Data mot_b_bemf_adc; // SE23

HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotA;
HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotB;

void HDU_POWERTRAIN_init(uint32_t frequencyHz)
{
//	HDU_ADC0_init();

	mot_a_cs_adc.channel = 3;
	mot_a_cs_adc.mux = kADC16_ChannelMuxA;
	mot_b_cs_adc.channel = 7;
	mot_b_cs_adc.mux = kADC16_ChannelMuxA;
	mot_a_bemf_adc.channel = 14;
	mot_a_bemf_adc.mux = kADC16_ChannelMuxA;
	mot_b_bemf_adc.channel = 23;
	mot_b_bemf_adc.mux = kADC16_ChannelMuxA;

    mot_a_pwm1.chnlNumber = (tpm_chnl_t)0U;
    mot_a_pwm1.level = kTPM_HighTrue;
    mot_a_pwm1.dutyCyclePercent = 0;

    mot_a_pwm2.chnlNumber = (tpm_chnl_t)1U;
    mot_a_pwm2.level = kTPM_HighTrue;
    mot_a_pwm2.dutyCyclePercent = 0;

    mot_b_pwm1.chnlNumber = (tpm_chnl_t)2U;
    mot_b_pwm1.level = kTPM_HighTrue;
    mot_b_pwm1.dutyCyclePercent = 0;

    mot_b_pwm2.chnlNumber = (tpm_chnl_t)3U;
    mot_b_pwm2.level = kTPM_HighTrue;
    mot_b_pwm2.dutyCyclePercent = 0;

    tpm_chnl_pwm_signal_param_t motorPWMConf[4];
    motorPWMConf[0] = mot_a_pwm1;
    motorPWMConf[1] = mot_a_pwm2;
    motorPWMConf[2] = mot_b_pwm1;
    motorPWMConf[3] = mot_b_pwm2;

    CLOCK_SetTpmClock(1U);
    TPM_GetDefaultConfig(&tpm0Conf);
    TPM_Init(TPM0, &tpm0Conf);
    TPM_SetupPwm(TPM0, motorPWMConf, 4U, kTPM_CenterAlignedPwm, frequencyHz, CLOCK_GetFreq(kCLOCK_PllFllSelClk));

//    TPM_EnableInterrupts(TPM0, kTPM_TimeOverflowInterruptEnable);
//    EnableIRQ(TPM0_IRQn);

    TPM_StartTimer(TPM0, kTPM_SystemClock);

    HDU_POWERTRAIN_enable();
}

void HDU_POWERTRAIN_updateMotorValuesA();
void HDU_POWERTRAIN_updateMotorValuesB();
void HDU_POWERTRAIN_updateMotorValues();

void HDU_POWERTRAIN_enable()
{
	GPIO_WritePinOutput(HDU_MOT_EN_GPIO, HDU_MOT_EN_PIN, 1);
}
void HDU_POWERTRAIN_disable()
{
	GPIO_WritePinOutput(HDU_MOT_EN_GPIO, HDU_MOT_EN_PIN, 0);
}

void HDU_POWERTRAIN_setDutycycleMotA(int dutyCycle)
{
		if(dutyCycle < 0)
		{
			dutyCycle = -dutyCycle;

	        TPM_UpdatePwmDutycycle(TPM0, mot_a_pwm2.chnlNumber, kTPM_CenterAlignedPwm, 0);
	        TPM_UpdatePwmDutycycle(TPM0, mot_a_pwm1.chnlNumber, kTPM_CenterAlignedPwm, dutyCycle);
		}
		else
		{
			TPM_UpdatePwmDutycycle(TPM0, mot_a_pwm1.chnlNumber, kTPM_CenterAlignedPwm, 0);
	        TPM_UpdatePwmDutycycle(TPM0, mot_a_pwm2.chnlNumber, kTPM_CenterAlignedPwm, dutyCycle);
		}
}

void HDU_POWERTRAIN_setDutycycleMotB(int dutyCycle)
{
		if(dutyCycle < 0)
		{
			dutyCycle = -dutyCycle;

	        TPM_UpdatePwmDutycycle(TPM0, mot_b_pwm2.chnlNumber, kTPM_CenterAlignedPwm, 0);
	        TPM_UpdatePwmDutycycle(TPM0, mot_b_pwm1.chnlNumber, kTPM_CenterAlignedPwm, dutyCycle);
		}
		else
		{
			TPM_UpdatePwmDutycycle(TPM0, mot_b_pwm1.chnlNumber, kTPM_CenterAlignedPwm, 0);
	        TPM_UpdatePwmDutycycle(TPM0, mot_b_pwm2.chnlNumber, kTPM_CenterAlignedPwm, dutyCycle);
		}
}

void HDU_POWERTRAIN_setDutycycles(int dutyCycleA, int dutyCycleB)
{
	HDU_POWERTRAIN_setDutycycleMotA(dutyCycleA);
	HDU_POWERTRAIN_setDutycycleMotB(dutyCycleB);
}

uint16_t HDU_POWERTRAIN_readCsA()
{
	HDU_ADC0_read(&mot_a_cs_adc);
	return mot_a_cs_adc.value;
}

uint16_t HDU_POWERTRAIN_readCsB()
{
	HDU_ADC0_read(&mot_b_cs_adc);
	return mot_b_cs_adc.value;
}

uint16_t HDU_POWERTRAIN_readBemfA()
{
	HDU_ADC0_read(&mot_a_bemf_adc);
	return mot_a_bemf_adc.value;
}

uint16_t HDU_POWERTRAIN_readBemfB()
{
	HDU_ADC0_read(&mot_b_bemf_adc);
	return mot_b_bemf_adc.value;
}

//void TPM0_IRQHandler()
//{
//	// do fancy back EMF stuff here ;-)
//}

