/*
 * HDU_BLUETOOTH.c
 *
 *  Created on: Aug 8, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include <HDU/HDU_BLUETOOTH.h>

uart_config_t bluetooth_uart_config;

void HDU_BLUETOOTH_init(uint32_t baudrate)
{
	  CLOCK_EnableClock(kCLOCK_PortE);

	  PORTE->PCR[0] = (PORTE->PCR[0] & ~PORT_PCR_MUX_MASK) | PORT_PCR_MUX(3U);

	  SIM->SOPT5 = ((SIM->SOPT5 &
	    (~(SIM_SOPT5_UART1TXSRC_MASK | SIM_SOPT5_UART1RXSRC_MASK)))
			  | SIM_SOPT5_UART1TXSRC(0));

	BOARD_BootClockRUN();

	UART_GetDefaultConfig(&bluetooth_uart_config);
    bluetooth_uart_config.baudRate_Bps = baudrate;
    bluetooth_uart_config.enableTx = true;
    bluetooth_uart_config.enableRx = false;

    UART_Init(UART1, &bluetooth_uart_config, CLOCK_GetFreq(BUS_CLK));
}

void HDU_BLUETOOTH_sendMessage(char *message)
{
	char str[40];
	int len = sprintf(str, "%s", message);
	UART_WriteBlocking(UART1, (uint8_t *)str, len);
}

void HDU_BLUETOOTH_sendDebugMesssage(char *message, float value, char *unit)
{
	char str[40];

	int len = sprintf(str, "%s: %d.%d%s\n", message, (int)value, (int)(value * 1000.0) % 1000, unit);

	UART_WriteBlocking(UART1, (uint8_t *)str, len);
}
