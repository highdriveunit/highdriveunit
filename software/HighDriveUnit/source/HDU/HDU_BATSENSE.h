/*
 * HDU_BATSENSE.h
 *
 *  Created on: Aug 8, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for reading battery voltage level
 */

#ifndef HDU_HDU_BATSENSE_H_
#define HDU_HDU_BATSENSE_H_

#include <HDU/HDU_ADC0.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

/*!
 * @brief Initializes battery sense reading.
 * @param r1 value of top voltage divider resistor
 * @param r2 value of bottom voltage divider resistor
 */
void HDU_BATSENSE_init(int r1, int r2);

/*!
 * @brief Reads the battery voltage adc.
 * @retval battery voltage in volts
 */
float HDU_BATSENSE_read();

#endif /* HDU_HDU_BATSENSE_H_ */
