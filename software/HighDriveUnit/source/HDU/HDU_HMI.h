/*
 * HDU_HMI.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for HMI ( LEDs, dipswitches, potis and buttons )
 */

#ifndef HDU_HDU_HMI_H_
#define HDU_HDU_HMI_H_

#include <HDU/HDU_ADC0.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

/*!
 * Hmi input data struct.
 * Stores poti values, dipswitch values and button states.
 */
typedef struct HDU_HMI_UserInput
{
	uint16_t poti_a;
	uint16_t poti_b;
	uint8_t dipswitches;
	bool buttonA;
	bool buttonB;
} HDU_HMI_UserInput;

extern HDU_HMI_UserInput HDU_HMI_userInput;

/*!
 * @brief Initializes HMI
 */
void HDU_HMI_init();

/*!
 * @brief Reads all data to user input struct
 */
void HDU_HMI_readUserInput();

/*
 * @brief Reads dipswitches using multiplexing
 * @retval 8-bit value of switches
 */
uint8_t HDU_HMI_readDipswitches();

/*!
 * @brief Reads button A
 * @retval state of button
 */
bool HDU_HMI_readBtnA();

/*!
 * @brief Reads button A
 * @retval state of button
 */
bool HDU_HMI_readBtnB();

/*!
 * @brief Reads potentiometer A ADC
 * @retval value of poti from 0 to fff
 */
uint16_t HDU_HMI_readPotA();

/*!
 * @brief Reads potentiometer B ADC
 * @retval value of poti from 0 to fff
 */
uint16_t HDU_HMI_readPotB();

/*!
 * @brief sets LEDs
 * @param value 4-bit value of LED states
 */
void HDU_HMI_setLEDs(uint8_t value);

#endif /* HDU_HDU_HMI_H_ */
