/*
 * ADC0.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Helper functions for reading values from all ADC0 single channels
 */

#ifndef HDU_HDU_ADC0_H_
#define HDU_HDU_ADC0_H_

#include "fsl_adc16.h"

/*
 * Struct for saving adc channel data
 * contains number of SE channel, mux mode and last value read
 */
typedef struct HDU_ADC0_Data{
	uint32_t channel;
	adc16_channel_mux_mode_t mux;
	uint16_t value;
} HDU_ADC0_Data;

/*!
 * @brief Initializes ADC0 in 16 bit mode
 */
void HDU_ADC0_init();

/*!
 * @brief Reads value from ADC channel.
 * @param data ADC channel data
 * @retval value of adc reading stored in the struct is also returned by the function.
 */
uint16_t HDU_ADC0_read(HDU_ADC0_Data *data);

#endif /* HDU_HDU_ADC0_H_ */
