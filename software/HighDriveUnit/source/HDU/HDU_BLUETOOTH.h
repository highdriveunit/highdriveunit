/*
 * HDU_BLUETOOTH.h
 *
 *  Created on: Aug 8, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for using a HC-05 / 06 bluetooth module for debugging ( currently only tx messages )
 *      The key for these modules is usually 1234.
 */

#ifndef HDU_HDU_BLUETOOTH_H_
#define HDU_HDU_BLUETOOTH_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_uart.h"

/*!
 * @brief Initializes UART1 in TX blocking mode.
 * @param baudrate  typically 9600
 */
void HDU_BLUETOOTH_init(uint32_t baudrate);

/*!
 * @brief Sends a string over bluetooth uart.
 * @param message takes c string as input
 */
void HDU_BLUETOOTH_sendMessage(char *message);

/*!
 * @brief Can be used to debug a variable in the format "message: value unit\n".
 * @param message for example variable name and description
 * @param value variable value
 * @param unit variable unit
 */
void HDU_BLUETOOTH_sendDebugMesssage(char *message, float value, char *unit);
#endif /* HDU_HDU_BLUETOOTH_H_ */
