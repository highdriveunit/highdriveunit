/*
 * HDU_LINESCAN.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 *
 *      Functions for reading Camera 0 and Camera 1
 */

#ifndef HDU_HDU_LINESCAN_H_
#define HDU_HDU_LINESCAN_H_

#include <HDU/HDU_ADC0.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_pit.h"

/*!
 * @brief  Struct for storing camera raw data in array.
 * Includes also bools telling if a frame is available and which cameras are enabled.
 *
 */
typedef struct HDU_LINESCAN_Data
{
	bool available;
	bool cam0En, cam1En;
	uint16_t pixels0[128];
	uint16_t pixels1[128];
} HDU_LINESCAN_Data;

/*!
 * @brief Initializes cameras and periodic interrupt timer for evaluation
 * @param periodUs evaluation period in microseconds
 */
void HDU_LINESCAN_init(uint64_t periodUs);

/*!
 * @brief Enables camera 0
 */
void HDU_LINESCAN_enable0();

/*!
 * @brief Enables camera 1
 */
void HDU_LINESCAN_enable1();

/*!
 * @brief Disables camera 0
 */
void HDU_LINESCAN_disable0();

/*!
 * @brief Disables camera 1
 */
void HDU_LINESCAN_disable1();

/*!
 * @brief waits for available frame and captures is
 * @retval camera frame data
 */
HDU_LINESCAN_Data HDU_LINESCAN_capture();

/*!
 * @brief reads camera data and is executed periodically by default.
 */
void HDU_LINESCAN_read();

#endif /* HDU_HDU_LINESCAN_H_ */
