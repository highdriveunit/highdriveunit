/*
 * HDU_SERVO.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_SERVO_H_
#define HDU_SERVO_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_tpm.h"

void HDU_SERVO_init(uint32_t frequencyHz);

void HDU_SERVO_0_setDutycycle(float dutycycle);
void HDU_SERVO_1_setDutycycle(float dutycycle);

void HDU_SERVO_0_setHeading(int heading);
void HDU_SERVO_1_setHeading(int heading);

#endif /* HDU_SERVO_H_ */
