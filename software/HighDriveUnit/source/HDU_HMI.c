/*
 * HDU_HMI.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include "HDU_HMI.h"

HDU_ADC0_Data pot_a_adc; // SE13
HDU_ADC0_Data pot_b_adc; // SE12

HDU_HMI_UserInput HDU_HMI_userInput;

void HDU_HMI_init()
{
	HDU_ADC0_init();

	pot_a_adc.channel = 13;
	pot_a_adc.mux = kADC16_ChannelMuxA;

	pot_b_adc.channel = 12;
	pot_b_adc.mux = kADC16_ChannelMuxB;

	HDU_HMI_readUserInput();
}

void HDU_HMI_readUserInput()
{
	HDU_HMI_userInput.buttonA = HDU_HMI_readBtnA();
	HDU_HMI_userInput.buttonB = HDU_HMI_readBtnB();

	HDU_HMI_userInput.poti_a = HDU_HMI_readPotA();
	HDU_HMI_userInput.poti_b = HDU_HMI_readPotB();

	HDU_HMI_userInput.dipswitches = HDU_HMI_readDipswitches();
}

uint8_t HDU_HMI_readDipswitches()
{
	GPIO_WritePinOutput(HDU_SW_2_EN_GPIO, HDU_SW_2_EN_PIN, 1);
	GPIO_WritePinOutput(HDU_SW_1_EN_GPIO, HDU_SW_1_EN_PIN, 0);

	uint8_t b4 = GPIO_ReadPinInput(HDU_SW_B3_GPIO, HDU_SW_B3_PIN);
	uint8_t b5 = GPIO_ReadPinInput(HDU_SW_B2_GPIO, HDU_SW_B2_PIN);
	uint8_t b6 = GPIO_ReadPinInput(HDU_SW_B1_GPIO, HDU_SW_B1_PIN);
	uint8_t b7 = GPIO_ReadPinInput(HDU_SW_B0_GPIO, HDU_SW_B0_PIN);

	GPIO_WritePinOutput(HDU_SW_2_EN_GPIO, HDU_SW_2_EN_PIN, 0);
	GPIO_WritePinOutput(HDU_SW_1_EN_GPIO, HDU_SW_1_EN_PIN, 1);

	uint8_t b3 = GPIO_ReadPinInput(HDU_SW_B3_GPIO, HDU_SW_B3_PIN);
	uint8_t b2 = GPIO_ReadPinInput(HDU_SW_B2_GPIO, HDU_SW_B2_PIN);
	uint8_t b1 = GPIO_ReadPinInput(HDU_SW_B1_GPIO, HDU_SW_B1_PIN);
	uint8_t b0 = GPIO_ReadPinInput(HDU_SW_B0_GPIO, HDU_SW_B0_PIN);


	return (b7 * 256 + b6 * 128 + b5 * 64 + b5 * 32 + b4 * 16 + b3 * 8 + b2 * 4
			+ b1 * 2 + b0);
}

bool HDU_HMI_readBtnA()
{
	return GPIO_ReadPinInput(HDU_BTN_A_GPIO, HDU_BTN_A_PIN);
}

bool HDU_HMI_readBtnB()
{
	return GPIO_ReadPinInput(HDU_BTN_B_GPIO, HDU_BTN_B_PIN);
}

uint16_t HDU_HMI_readPotA()
{
	HDU_ADC0_read(&pot_a_adc);
	return pot_a_adc.value;
}

uint16_t HDU_HMI_readPotB()
{
	HDU_ADC0_read(&pot_b_adc);
	return pot_b_adc.value;
}

void HDU_HMI_setLEDs(uint8_t value)
{
	GPIO_WritePinOutput(HDU_LED_1_GPIO, HDU_LED_1_PIN, value & 0x01);
	value = value >> 1;
	GPIO_WritePinOutput(HDU_LED_2_GPIO, HDU_LED_2_PIN, value & 0x01);
	value = value >> 1;
	GPIO_WritePinOutput(HDU_LED_3_GPIO, HDU_LED_3_PIN, value & 0x01);
	value = value >> 1;
	GPIO_WritePinOutput(HDU_LED_4_GPIO, HDU_LED_4_PIN, value & 0x01);
}
