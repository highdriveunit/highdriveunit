/*
 * HDU_HMI.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_HMI_H_
#define HDU_HMI_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "HDU_ADC0.h"

typedef struct HDU_HMI_UserInput
{
	uint16_t poti_a;
	uint16_t poti_b;
	uint8_t dipswitches;
	bool buttonA;
	bool buttonB;
} HDU_HMI_UserInput;

extern HDU_HMI_UserInput HDU_HMI_userInput;

void HDU_HMI_init();

void HDU_HMI_readUserInput();

uint8_t HDU_HMI_readDipswitches();

bool HDU_HMI_readBtnA();
bool HDU_HMI_readBtnB();

uint16_t HDU_HMI_readPotA();
uint16_t HDU_HMI_readPotB();

void HDU_HMI_setLEDs(uint8_t value);

#endif /* HDU_HMI_H_ */
