/*
 * HDU_POWERTRAIN.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_POWERTRAIN_H_
#define HDU_POWERTRAIN_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_tpm.h"

#include "HDU_ADC0.h"

typedef struct HDU_POWERTRAIN_MotorValues{
	int dutyCycle;
	uint16_t currentSense, backEMF;
}HDU_POWERTRAIN_MotorValues;

extern HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotA;
extern HDU_POWERTRAIN_MotorValues HDU_POWERTRAIN_MotB;

void HDU_POWERTRAIN_init(uint32_t frequencyHz);

void HDU_POWERTRAIN_updateMotorValuesA();
void HDU_POWERTRAIN_updateMotorValuesB();
void HDU_POWERTRAIN_updateMotorValues();

void HDU_POWERTRAIN_enable();
void HDU_POWERTRAIN_disable();

void HDU_POWERTRAIN_setDutycycleMotA(int dutyCycle);
void HDU_POWERTRAIN_setDutycycleMotB(int dutyCycle);
void HDU_POWERTRAIN_setDutycycles(int dutyCycleA, int dutyCycleB);

uint16_t HDU_POWERTRAIN_readCsA();
uint16_t HDU_POWERTRAIN_readCsB();

uint16_t HDU_POWERTRAIN_readBemfA();
uint16_t HDU_POWERTRAIN_readBemfB();


#endif /* HDU_POWERTRAIN_H_ */
