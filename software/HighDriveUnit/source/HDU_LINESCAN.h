/*
 * HDU_LINESCAN.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_LINESCAN_H_
#define HDU_LINESCAN_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "fsl_pit.h"

#include "HDU_ADC0.h"

typedef struct HDU_LINESCAN_Data
{
	bool available;
	bool cam0En, cam1En;
	uint16_t pixels0[128];
	uint16_t pixels1[128];
} HDU_LINESCAN_Data;

void HDU_LINESCAN_init(uint64_t periodUs);

void HDU_LINESCAN_enable0();
void HDU_LINESCAN_enable1();

void HDU_LINESCAN_disable0();
void HDU_LINESCAN_disable1();

HDU_LINESCAN_Data HDU_LINESCAN_capture();

void HDU_LINESCAN_read();

#endif /* HDU_LINESCAN_H_ */
