/*
 * HDU_Core.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_CORE_H_
#define HDU_CORE_H_

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"

#include "HDU_ADC0.h"
#include "HDU_HMI.h"
#include "HDU_POWERTRAIN.h"
#include "HDU_LINESCAN.h"
#include "HDU_SERVO.h"

void HDU_init();
#endif /* HDU_CORE_H_ */
