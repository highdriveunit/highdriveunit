/*
 * Copyright 2016-2020 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    HighDriveUnit.c
 * @brief   Application entry point.
 */

#include <HDU/HDU.h>
/* TODO: insert other include files here. */



/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
void testOutput(GPIO_Type *base, uint32_t pin)
{
	GPIO_WritePinOutput(base, pin, 0);
	for(int i = 0; i < 1000000; i++){};
	GPIO_WritePinOutput(base, pin, 1);
	for(int i = 0; i < 1000000; i++){};
}
float servodutycycle;
int main(void) {

	HDU_init();

    while(1) {
    	HDU_HMI_readUserInput();
    	HDU_HMI_setLEDs(HDU_HMI_userInput.dipswitches);
    	int dutycycle = (float)HDU_HMI_userInput.poti_a / 4096 * 100;
    	HDU_POWERTRAIN_setDutycycles(dutycycle, dutycycle);
    	servodutycycle = 0.05 + ((float)HDU_HMI_userInput.poti_b * (0.05/4096));
    	HDU_SERVO_0_setDutycycle(servodutycycle);
    	HDU_SERVO_1_setDutycycle(servodutycycle);
    }
    return 0 ;
}
