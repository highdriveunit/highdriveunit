/*
 * HDU_Core.c
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */
#include "HDU_Core.h"

void HDU_init()
{
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

	HDU_HMI_init();
	HDU_POWERTRAIN_init(3000);
	HDU_LINESCAN_init(50000);
	HDU_SERVO_init(50);
}


