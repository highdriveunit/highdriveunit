/*
 * ADC0.h
 *
 *  Created on: Aug 7, 2020
 *      Author: Jonas Wuehr ( 00753979 )
 */

#ifndef HDU_ADC0_H_
#define HDU_ADC0_H_

#include "fsl_adc16.h"

typedef struct HDU_ADC0_Data{
	uint32_t channel;
	adc16_channel_mux_mode_t mux;
	uint16_t value;
} HDU_ADC0_Data;

void HDU_ADC0_init();

uint16_t HDU_ADC0_read(HDU_ADC0_Data *data);

#endif /* HDU_ADC0_H_ */
