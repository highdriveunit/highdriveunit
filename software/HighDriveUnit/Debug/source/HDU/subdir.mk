################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/HDU/HDU.c \
../source/HDU/HDU_ADC0.c \
../source/HDU/HDU_BATSENSE.c \
../source/HDU/HDU_BLUETOOTH.c \
../source/HDU/HDU_HMI.c \
../source/HDU/HDU_LINESCAN.c \
../source/HDU/HDU_POWERTRAIN.c \
../source/HDU/HDU_SERVO.c 

OBJS += \
./source/HDU/HDU.o \
./source/HDU/HDU_ADC0.o \
./source/HDU/HDU_BATSENSE.o \
./source/HDU/HDU_BLUETOOTH.o \
./source/HDU/HDU_HMI.o \
./source/HDU/HDU_LINESCAN.o \
./source/HDU/HDU_POWERTRAIN.o \
./source/HDU/HDU_SERVO.o 

C_DEPS += \
./source/HDU/HDU.d \
./source/HDU/HDU_ADC0.d \
./source/HDU/HDU_BATSENSE.d \
./source/HDU/HDU_BLUETOOTH.d \
./source/HDU/HDU_HMI.d \
./source/HDU/HDU_LINESCAN.d \
./source/HDU/HDU_POWERTRAIN.d \
./source/HDU/HDU_SERVO.d 


# Each subdirectory must supply rules for building sources it contributes
source/HDU/%.o: ../source/HDU/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MKL25Z128VLK4 -DCPU_MKL25Z128VLK4_cm0plus -DSDK_OS_BAREMETAL -DFSL_RTOS_BM -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DSDK_DEBUGCONSOLE_UART -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/jonas/Documents/wsNxp/HighDriveUnit/drivers" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/utilities" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/CMSIS" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/drivers" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/utilities" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/CMSIS" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/board" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/source" -I"/home/jonas/Documents/wsNxp/HighDriveUnit" -I"/home/jonas/Documents/wsNxp/HighDriveUnit/startup" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m0plus -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


