#!/bin/bash
#set -x
#trap read debug
usage()
{
  echo "Usage:"
  echo '$0 "X.X" "Commit message" { PUSH | NOPUSH } SHA1'
  echo "The first argument is the revision tag used to mark revisions to the gitlab system."
  echo "It is appended to rev'. Add the tag and a description to the wiki."
  echo "The second argument is the the commit message used."
  echo "Only if PUSH is given anything is pushed. Use with care"
  echo "SHA1 is taken as the the commit on master to be released use 'origin/master' for the most recent version."
}

wrong_args()
{
  echo "Wrong arguments. Abort"
  usage
  exit 1
}

exiting_err()
{
  echo "something went wrong here..."
  echo "heres a shell to inspect"
  set +e
  trap - EXIT
  bash
  cd ../../
  rm -rf $DIR
  exit 2
}

exiting_norm()
{
  echo "Nothing pushed, starting a shell to look around."
  set +e
  trap - EXIT
  bash
  cd ../../
  rm -rf $DIR
  exit 0
}

case "$#" in
  "4")
    if [[ ( "$3" == "PUSH" ) || ( "$3" == "NOPUSH" ) ]]
    then
      VER="$1"
      MESSAGE="$2"
      PUSH="$3"
      SHALIKE="$4"
    else
      wrong_args
    fi
    ;;
  *)
    wrong_args
    ;;
esac


TAG="rev${VER}"
STDREPO=git@mygit.th-deg.de:highdriveunit/highdriveunit.git
MIRREPO=git@gitlab.com:highdriveunit/highdriveunit.git
REPONAME=highdriveunit


set -e
trap "exiting_err" EXIT


DIR=$(mktemp -d)
echo "Workdir $DIR"
cd $DIR
git clone ${STDREPO}
cd $REPONAME

echo "Add mirror as remote"
git remote add mirror ${MIRREPO}

echo "Fetch from mirror"
git fetch mirror



echo "Check out branch release"
git reset --hard origin/release
git checkout -b release

echo "Merge commit only accepting the master branches versions (copying)"
git merge    --squash --strategy=recursive --strategy-option=theirs "${SHALIKE}"

echo "Comitting the merge"
git commit   --message "${MESSAGE}"

echo "Creating the tag"
git tag      --annotate --message "${TAG}" "${TAG}"

if [[ "$PUSH" == "PUSH" ]]
then
  echo "Pushing branch to origin"
  git push origin release
  echo "Pushing tag to origin"
  git push origin "${TAG}"
  echo "Pushing branch to mirror"
  git push mirror release
  echo "Pushing tag to mirror"
  git push mirror "${TAG}"
  set +e
  trap - EXIT
else
  echo "Nothing pushed, starting a shell to look around."
  set +e
  trap - EXIT
  bash
fi

cd ../../
rm -rf $DIR

exit 0
