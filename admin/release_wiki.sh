#!/bin/bash
#set -x
#trap read debug
usage()
{
  echo "Usage:"
  echo '$0 "Commit message" { PUSH | NOPUSH } SHA1'
  echo "The first argument is the the commit message used."
  echo "Only if PUSH is given anything is pushed. Use with care"
  echo "SHA1 is taken as the the commit on master to be released use 'origin/master' for the most recent version."
}

wrong_args()
{
  echo "Wrong arguments. Abort"
  usage
  exit 1
}

exiting_err()
{
  echo "something went wrong here..."
  echo "heres a shell to inspect"
  set +e
  trap - EXIT
  bash
  cd /
  rm -rf $DIR
  exit 2
}

exiting_norm()
{
  echo "Nothing pushed, starting a shell to look around."
  set +e
  trap - EXIT
  bash
  cd /
  rm -rf $DIR
  exit 0
}

case "$#" in
  "3")
    if [[ ( "$2" == "PUSH" ) || ( "$2" == "NOPUSH" ) ]]
    then
      MESSAGE="$1"
      PUSH="$2"
      SHALIKE="$3"
    else
      wrong_args
    fi
    ;;
  *)
    wrong_args
    ;;
esac


STDREPO=git@mygit.th-deg.de:highdriveunit/highdriveunit.wiki.git
MIRREPO=git@gitlab.com:highdriveunit/highdriveunit.wiki.git
REPONAME=highdriveunit.wiki


set -e
trap "exiting_err" EXIT


DIR=$(mktemp -d)
echo "Workdir $DIR"
cd $DIR
git clone ${STDREPO}
cd $REPONAME

echo "Add mirror as remote"
git remote add mirror ${MIRREPO}

echo "Fetch from mirror"
git fetch mirror

echo "Check out branch release"
git reset --hard mirror/master
git checkout -b mirrorbr

echo "Merge commit only accepting the master branches versions (copying)"
git merge --squash --strategy=recursive --strategy-option=theirs "${SHALIKE}"

echo "Comitting the merge"
git commit --message "${MESSAGE}"

if [[ "$PUSH" == "PUSH" ]]
then
  echo "Pushing to mirror"
  git push mirror mirrorbr:master
  set +e
  trap - EXIT
else
  echo "Nothing pushed, starting a shell to look around."
  set +e
  trap - EXIT
  (bash)
fi

cd /
rm -rf $DIR

exit 0
