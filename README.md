# HighDriveUnit

[![pipeline status](https://mygit.th-deg.de/highdriveunit/highdriveunit/badges/master/pipeline.svg)](https://mygit.th-deg.de/highdriveunit/highdriveunit/-/commits/master)

**see ![wiki](https://mygit.th-deg.de/highdriveunit/highdriveunit/-/wikis/home) for complete documentation**

![HighdriveUnit overview](images/overview.jpg)

The HighDriveUnit is a completely new developed cape for FRDM(-KL25Z) featuring everything needed for autonomously driving robots. It's main application is the control of NXP-Cup platforms with DC motors. For this reason the nxpboard is fully compatible with the features of the FRDM-TFC shield by Waveshare. 


## Features
* Power input with PTC fuse and reverse polarity protection ( 6 - 15V DC)
* Control of two DC motors (up to approximately 20A) with current sense, motor fault detection and back emf measurement
* Control of two Servos at 6V DC with adjustable maximum current
* HMI with 2 potentiometers, OLED Display connection, 8 dipswitches, 4 LEDs and two push buttons
* Reset button
* Power control LEDS (3V3, 6V and VBAT)
* SD-Card slot
* Connector for HC-SR05/6 Bluetooth module
* Connectors for 2 Freescale linescan cameras
* Connectors for 2 analog encoders
* Breakout headers for breadboard
* ID field

![Evalboard mounting](images/evalboard_mounting.jpg)
![Bluetooth module mounting](images/bluetooth mounting.jpg)
