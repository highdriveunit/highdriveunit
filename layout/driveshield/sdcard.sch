EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L driveshield-rescue:+3V3-power-supply #PWR088
U 1 1 5EAEF36A
P 4350 3050
F 0 "#PWR088" H 4350 2900 50  0001 C CNN
F 1 "+3V3" H 4365 3223 50  0000 C CNN
F 2 "" H 4350 3050 50  0000 C CNN
F 3 "" H 4350 3050 50  0000 C CNN
	1    4350 3050
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR087
U 1 1 5EAEF505
P 4300 4200
F 0 "#PWR087" H 4300 3950 50  0001 C CNN
F 1 "GND" H 4305 4027 50  0000 C CNN
F 2 "" H 4300 4200 50  0000 C CNN
F 3 "" H 4300 4200 50  0000 C CNN
	1    4300 4200
	1    0    0    -1  
$EndComp
Text GLabel 4200 3350 0    50   Input ~ 0
SCK_0
$Comp
L driveshield-rescue:Micro_SDCard-electrical-connectors CON1
U 1 1 5EAF2900
P 5650 3800
F 0 "CON1" H 6828 4023 50  0000 L CNN
F 1 "Micro_SDCard" H 6828 3925 59  0000 L CNN
F 2 "CON_wuerth:WE_693072010801" H 6828 3827 50  0000 L CNN
F 3 "" H 5850 3800 60  0000 C CNN
F 4 "693072010801" H 5650 3800 50  0001 C CNN "manf#"
F 5 "Vfull" H 5650 3800 50  0001 C CNN "variant"
F 6 "1" H 5650 3800 50  0001 C CNN "check"
	1    5650 3800
	1    0    0    -1  
$EndComp
Text GLabel 4200 4000 0    50   Output ~ 0
MISO_0
Text GLabel 4200 3600 0    50   Input ~ 0
MOSI_0
Wire Wire Line
	5000 3700 4600 3700
Wire Wire Line
	5000 3900 4600 3900
Wire Wire Line
	5000 3500 4200 3500
Text GLabel 4200 3500 0    50   Input ~ 0
CS_SD
Wire Wire Line
	4350 3050 4350 3700
Wire Wire Line
	4200 3600 5000 3600
Wire Wire Line
	4200 4000 5000 4000
Wire Wire Line
	4300 3900 4300 4200
NoConn ~ 5000 4100
NoConn ~ 5000 3400
$Comp
L driveshield-rescue:C_0603-devices C45
U 1 1 5F093624
P 4600 3800
F 0 "C45" H 4350 3850 50  0000 L CNN
F 1 "CAP CER 100nF 6V 0603" H 3550 3750 50  0000 L CNN
F 2 "capacitors:C_0603" H 4600 3650 50  0001 C CNN
F 3 "" H 4600 3800 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 4600 3800 50  0001 C CNN "manf#"
F 5 "1" H 4600 3800 50  0001 C CNN "check"
	1    4600 3800
	1    0    0    -1  
$EndComp
Connection ~ 4600 3700
Wire Wire Line
	4600 3700 4350 3700
Connection ~ 4600 3900
Wire Wire Line
	4600 3900 4300 3900
Wire Wire Line
	4200 3350 4850 3350
Wire Wire Line
	4850 3350 4850 3800
Wire Wire Line
	4850 3800 5000 3800
$EndSCHEMATC
