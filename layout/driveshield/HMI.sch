EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1550 650  2    50   ~ 0
Push Buttons
$Comp
L driveshield-rescue:C_0603-devices C43
U 1 1 5EA97457
P 3250 2100
F 0 "C43" V 3021 2100 50  0000 C CNN
F 1 "CAP CER 100nF 6V 0603" V 3112 2100 50  0000 C CNN
F 2 "capacitors:C_0603" H 3250 1950 50  0001 C CNN
F 3 "" H 3250 2100 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 3250 2100 50  0001 C CNN "manf#"
F 5 "1" H 3250 2100 50  0001 C CNN "check"
	1    3250 2100
	0    1    1    0   
$EndComp
Text GLabel 3050 1400 2    50   Output ~ 0
PUSH_BTN_A
$Comp
L driveshield-rescue:GND-power-supply #PWR068
U 1 1 5EA92FB5
P 3700 2250
F 0 "#PWR068" H 3700 2000 50  0001 C CNN
F 1 "GND" H 3705 2077 50  0000 C CNN
F 2 "" H 3700 2250 50  0000 C CNN
F 3 "" H 3700 2250 50  0000 C CNN
	1    3700 2250
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR063
U 1 1 5EA90544
P 1000 850
F 0 "#PWR063" H 1000 700 50  0001 C CNN
F 1 "+3V3" H 1015 1023 50  0000 C CNN
F 2 "" H 1000 850 50  0000 C CNN
F 3 "" H 1000 850 50  0000 C CNN
	1    1000 850 
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR067
U 1 1 5EAB4FB3
P 2350 3350
F 0 "#PWR067" H 2350 3200 50  0001 C CNN
F 1 "+3V3" H 2365 3523 50  0000 C CNN
F 2 "" H 2350 3350 50  0000 C CNN
F 3 "" H 2350 3350 50  0000 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3850 3100 3850
Wire Wire Line
	3300 3950 3100 3950
Text GLabel 5100 3050 2    50   Output ~ 0
DIP_SW_1
Wire Wire Line
	5050 3050 5100 3050
Text GLabel 5100 5300 2    50   Output ~ 0
DIP_SW_4
Connection ~ 5050 5150
Wire Wire Line
	5050 5150 5050 5300
Wire Wire Line
	5050 5300 5100 5300
Text GLabel 5100 4550 2    50   Output ~ 0
DIP_SW_3
Connection ~ 5050 4400
Wire Wire Line
	5050 4400 5050 4550
Wire Wire Line
	5050 4550 5100 4550
$Comp
L driveshield-rescue:SW_PUSH-devices SW2
U 1 1 5EA8ECFB
P 1850 1800
F 0 "SW2" H 1850 2145 50  0000 C CNN
F 1 "NO" H 1850 2054 50  0000 C CNN
F 2 "mechanical-switches:smd_push" H 1850 1963 50  0000 C CNN
F 3 "" H 1850 1800 50  0000 C CNN
F 4 "FSMJMLPTR" H 1850 1800 50  0001 C CNN "manf#"
F 5 "1" H 1850 1800 50  0001 C CNN "check"
	1    1850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3800 5100 3800
Wire Wire Line
	5050 3650 5050 3800
Connection ~ 5050 3650
Text GLabel 5100 3800 2    50   Output ~ 0
DIP_SW_2
Text Notes 2400 2950 2    50   ~ 0
Dip Switches
$Comp
L driveshield-rescue:POT-devices RV2
U 1 1 5EB6F081
P 1850 5300
F 0 "RV2" V 1804 5223 50  0000 R CNN
F 1 "Pot 100k SMD" V 1895 5223 50  0000 R CNN
F 2 "potentiometers:Potentiometer_Bourns_3339W_Angular_ScrewFront" V 1941 5222 50  0001 R CNN
F 3 "" H 1850 5300 50  0000 C CNN
F 4 "3306W-1-104" H 1850 5300 50  0001 C CNN "manf#"
F 5 "1" H 1850 5300 50  0001 C CNN "check"
	1    1850 5300
	0    1    -1   0   
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR064
U 1 1 5EB709ED
P 1250 5050
F 0 "#PWR064" H 1250 4900 50  0001 C CNN
F 1 "+3V3" H 1265 5223 50  0000 C CNN
F 2 "" H 1250 5050 50  0000 C CNN
F 3 "" H 1250 5050 50  0000 C CNN
	1    1250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5050 1850 5050
Wire Wire Line
	2000 5300 2150 5300
Wire Wire Line
	2150 5300 2150 5350
Connection ~ 2150 5550
Wire Wire Line
	2150 5550 1850 5550
$Comp
L driveshield-rescue:C_0603-devices C39
U 1 1 5EB77BF7
P 2150 5450
F 0 "C39" H 2000 5400 50  0000 C CNN
F 1 "CAP CER 100nF 6V 0603" H 2000 5500 50  0000 C CNN
F 2 "capacitors:C_0603" H 2150 5300 50  0001 C CNN
F 3 "" H 2150 5450 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 2150 5450 50  0001 C CNN "manf#"
F 5 "1" H 2150 5450 50  0001 C CNN "check"
	1    2150 5450
	-1   0    0    1   
$EndComp
Text GLabel 2200 5300 2    50   Output ~ 0
POT_A
Wire Wire Line
	2200 5300 2150 5300
Connection ~ 2150 5300
Connection ~ 1850 5050
$Comp
L driveshield-rescue:GND-power-supply #PWR066
U 1 1 5EBA1926
P 3100 5600
F 0 "#PWR066" H 3100 5350 50  0001 C CNN
F 1 "GND" H 3105 5427 50  0000 C CNN
F 2 "" H 3100 5600 50  0000 C CNN
F 3 "" H 3100 5600 50  0000 C CNN
	1    3100 5600
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:POT-devices RV3
U 1 1 5EBA192C
P 2800 5300
F 0 "RV3" V 2754 5223 50  0000 R CNN
F 1 "Pot 100k SMD" V 2845 5223 50  0000 R CNN
F 2 "potentiometers:Potentiometer_Bourns_3339W_Angular_ScrewFront" V 2891 5222 50  0001 R CNN
F 3 "" H 2800 5300 50  0000 C CNN
F 4 "3306W-1-104" H 2800 5300 50  0001 C CNN "manf#"
F 5 "1" H 2800 5300 50  0001 C CNN "check"
	1    2800 5300
	0    1    -1   0   
$EndComp
Wire Wire Line
	2950 5300 3100 5300
Wire Wire Line
	3100 5300 3100 5350
Connection ~ 3100 5550
Wire Wire Line
	3100 5550 2800 5550
Wire Wire Line
	3100 5600 3100 5550
$Comp
L driveshield-rescue:C_0603-devices C41
U 1 1 5EBA1938
P 3100 5450
F 0 "C41" H 2950 5400 50  0000 C CNN
F 1 "CAP CER 100nF 6V 0603" H 2950 5500 50  0000 C CNN
F 2 "capacitors:C_0603" H 3100 5300 50  0001 C CNN
F 3 "" H 3100 5450 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 3100 5450 50  0001 C CNN "manf#"
F 5 "1" H 3100 5450 50  0001 C CNN "check"
	1    3100 5450
	-1   0    0    1   
$EndComp
Text GLabel 3150 5300 2    50   Output ~ 0
POT_B
Wire Wire Line
	3150 5300 3100 5300
Connection ~ 3100 5300
Text Notes 2000 4900 2    50   ~ 0
Potentiometers
$Comp
L driveshield-rescue:GND-power-supply #PWR069
U 1 1 5EBB8FEF
P 5600 5200
F 0 "#PWR069" H 5600 4950 50  0001 C CNN
F 1 "GND" H 5605 5027 50  0000 C CNN
F 2 "" H 5600 5200 50  0000 C CNN
F 3 "" H 5600 5200 50  0000 C CNN
	1    5600 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5550 2800 5550
Connection ~ 2800 5550
Wire Wire Line
	2500 6050 2900 6050
$Comp
L driveshield-rescue:GND-power-supply #PWR065
U 1 1 5EBD5224
P 2900 7100
F 0 "#PWR065" H 2900 6850 50  0001 C CNN
F 1 "GND" H 2905 6927 50  0000 C CNN
F 2 "" H 2900 7100 50  0000 C CNN
F 3 "" H 2900 7100 50  0000 C CNN
	1    2900 7100
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:Led_Small-devices D3
U 1 1 5EBD7B33
P 2400 6050
F 0 "D3" H 2400 5845 50  0000 C CNN
F 1 "Led_Small" H 2400 5936 50  0000 C CNN
F 2 "LEDs:LED_1206" H 2400 5937 50  0001 C CNN
F 3 "" V 2400 6050 50  0000 C CNN
F 4 "LTST-C150GKT" H 2400 6050 50  0001 C CNN "manf#"
F 5 "1" H 2400 6050 50  0001 C CNN "check"
	1    2400 6050
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R51
U 1 1 5EBD9448
P 2100 6050
F 0 "R51" V 2296 6050 50  0000 C CNN
F 1 "RES 220 10% 0603" V 2205 6050 50  0000 C CNN
F 2 "resistors:R_0603" H 2100 5900 50  0001 C CNN
F 3 "" H 2100 6050 50  0000 C CNN
F 4 "RC0603FR-07220RL" H 2100 6050 50  0001 C CNN "manf#"
F 5 "1" H 2100 6050 50  0001 C CNN "check"
	1    2100 6050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 6050 2200 6050
Text GLabel 1900 6050 0    50   Input ~ 0
LED_1
Wire Wire Line
	2000 6050 1900 6050
Text Notes 1350 6100 2    50   ~ 0
LEDs
Wire Wire Line
	2500 6400 2900 6400
$Comp
L driveshield-rescue:Led_Small-devices D4
U 1 1 5EC4DAE6
P 2400 6400
F 0 "D4" H 2400 6195 50  0000 C CNN
F 1 "Led_Small" H 2400 6286 50  0000 C CNN
F 2 "LEDs:LED_1206" H 2400 6287 50  0001 C CNN
F 3 "" V 2400 6400 50  0000 C CNN
F 4 "LTST-C150GKT" H 2400 6400 50  0001 C CNN "manf#"
F 5 "1" H 2400 6400 50  0001 C CNN "check"
	1    2400 6400
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R52
U 1 1 5EC4DAEC
P 2100 6400
F 0 "R52" V 2296 6400 50  0000 C CNN
F 1 "RES 220 10% 0603" V 2205 6400 50  0000 C CNN
F 2 "resistors:R_0603" H 2100 6250 50  0001 C CNN
F 3 "" H 2100 6400 50  0000 C CNN
F 4 "RC0603FR-07220RL" H 2100 6400 50  0001 C CNN "manf#"
F 5 "1" H 2100 6400 50  0001 C CNN "check"
	1    2100 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 6400 2200 6400
Text GLabel 1900 6400 0    50   Input ~ 0
LED_2
Wire Wire Line
	2000 6400 1900 6400
Wire Wire Line
	2500 6750 2900 6750
$Comp
L driveshield-rescue:Led_Small-devices D5
U 1 1 5EC5C8E0
P 2400 6750
F 0 "D5" H 2400 6545 50  0000 C CNN
F 1 "Led_Small" H 2400 6636 50  0000 C CNN
F 2 "LEDs:LED_1206" H 2400 6637 50  0001 C CNN
F 3 "" V 2400 6750 50  0000 C CNN
F 4 "LTST-C150GKT" H 2400 6750 50  0001 C CNN "manf#"
F 5 "1" H 2400 6750 50  0001 C CNN "check"
	1    2400 6750
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R53
U 1 1 5EC5C8E6
P 2100 6750
F 0 "R53" V 2296 6750 50  0000 C CNN
F 1 "RES 220 10% 0603" V 2205 6750 50  0000 C CNN
F 2 "resistors:R_0603" H 2100 6600 50  0001 C CNN
F 3 "" H 2100 6750 50  0000 C CNN
F 4 "RC0603FR-07220RL" H 2100 6750 50  0001 C CNN "manf#"
F 5 "1" H 2100 6750 50  0001 C CNN "check"
	1    2100 6750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 6750 2200 6750
Text GLabel 1900 6750 0    50   Input ~ 0
LED_3
Wire Wire Line
	2000 6750 1900 6750
Wire Wire Line
	2500 7100 2900 7100
$Comp
L driveshield-rescue:Led_Small-devices D6
U 1 1 5EC5C8F0
P 2400 7100
F 0 "D6" H 2400 6895 50  0000 C CNN
F 1 "Led_Small" H 2400 6986 50  0000 C CNN
F 2 "LEDs:LED_1206" H 2400 6987 50  0001 C CNN
F 3 "" V 2400 7100 50  0000 C CNN
F 4 "LTST-C150GKT" H 2400 7100 50  0001 C CNN "manf#"
F 5 "1" H 2400 7100 50  0001 C CNN "check"
	1    2400 7100
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R54
U 1 1 5EC5C8F6
P 2100 7100
F 0 "R54" V 2296 7100 50  0000 C CNN
F 1 "RES 220 10% 0603" V 2205 7100 50  0000 C CNN
F 2 "resistors:R_0603" H 2100 6950 50  0001 C CNN
F 3 "" H 2100 7100 50  0000 C CNN
F 4 "RC0603FR-07220RL" H 2100 7100 50  0001 C CNN "manf#"
F 5 "1" H 2100 7100 50  0001 C CNN "check"
	1    2100 7100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 7100 2200 7100
Text GLabel 1900 7100 0    50   Input ~ 0
LED_4
Wire Wire Line
	2000 7100 1900 7100
Connection ~ 2900 6750
Wire Wire Line
	2900 6750 2900 7100
Connection ~ 2900 7100
Wire Wire Line
	2900 6050 2900 6400
Connection ~ 2900 6400
Wire Wire Line
	2900 6400 2900 6750
Wire Notes Line
	5900 800  5900 7300
$Comp
L driveshield-rescue:JUMPER3-devices JP2
U 1 1 5EA69220
P 2100 3450
F 0 "JP2" H 2100 3689 50  0000 C CNN
F 1 "CONN_01X03" H 2100 3598 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2100 3597 50  0001 C CNN
F 3 "" H 2100 3450 50  0000 C CNN
	1    2100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3350 2350 3450
Wire Wire Line
	3100 3750 3300 3750
Wire Wire Line
	3100 4050 3300 4050
Connection ~ 3100 3850
Wire Wire Line
	3100 3850 3100 3950
Connection ~ 3100 3950
Wire Wire Line
	3100 3950 3100 4050
Connection ~ 3100 3750
Wire Wire Line
	3100 3750 3100 3850
Text GLabel 1800 3450 0    50   Input ~ 0
SW1_EN
$Comp
L driveshield-rescue:SW_DIP_x4-mechanical-switches S2
U 1 1 5ECB8BC0
P 8650 2100
F 0 "S2" H 8650 2565 50  0000 C CNN
F 1 "SW_DIP" H 8650 2450 50  0000 C CNN
F 2 "mechanical-switches:dip_4-300" H 8600 2100 50  0001 C CNN
F 3 "http://www.ctscorp.com/components/Datasheets/206-208.pdf" H 8650 2383 50  0001 C CNN
F 4 "208-4" H 8650 2100 50  0001 C CNN "manf#"
F 5 "1" H 8650 2100 50  0001 C CNN "check"
	1    8650 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2250 10150 2250
Wire Wire Line
	8350 2250 8000 2250
Wire Wire Line
	8000 2250 8000 2150
Wire Wire Line
	8000 1950 8350 1950
Wire Wire Line
	8350 2050 8000 2050
Connection ~ 8000 2050
Wire Wire Line
	8000 2050 8000 1950
Wire Wire Line
	8350 2150 8000 2150
Connection ~ 8000 2150
Wire Wire Line
	8000 2150 8000 2050
Wire Wire Line
	9550 1950 10150 1950
Wire Wire Line
	10150 2150 9550 2150
Wire Wire Line
	9550 2050 10150 2050
Text Label 4700 2900 0    50   ~ 0
SW_1
Text Label 4800 4400 0    50   ~ 0
SW_3
Text Label 4750 5150 0    50   ~ 0
SW_4
Text Label 9950 1950 0    50   ~ 0
SW_1
Text Label 9950 2050 0    50   ~ 0
SW_2
Text Label 9950 2150 0    50   ~ 0
SW_3
Text Label 9950 2250 0    50   ~ 0
SW_4
$Comp
L driveshield-rescue:SW_PUSH-devices SW3
U 1 1 5EDC00D8
P 8500 3750
F 0 "SW3" H 8500 4095 50  0000 C CNN
F 1 "NO" H 8500 4004 50  0000 C CNN
F 2 "mechanical-switches:smd_push" H 8500 3913 50  0000 C CNN
F 3 "" H 8500 3750 50  0000 C CNN
F 4 "FSMJMLPTR" H 8500 3750 50  0001 C CNN "manf#"
F 5 "1" H 8500 3750 50  0001 C CNN "check"
	1    8500 3750
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R67
U 1 1 5EDC5204
P 7850 3650
F 0 "R67" H 7909 3696 50  0000 L CNN
F 1 "RES 10k 10% 0603" H 7909 3605 50  0000 L CNN
F 2 "resistors:R_0603" H 7850 3500 50  0001 C CNN
F 3 "" H 7850 3650 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 7850 3650 50  0001 C CNN "manf#"
F 5 "1" H 7850 3650 50  0001 C CNN "check"
	1    7850 3650
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C48
U 1 1 5EDC6731
P 8450 4000
F 0 "C48" H 8542 4046 50  0000 L CNN
F 1 "CAP CER 100nF 6V 0603" H 8542 3955 50  0000 L CNN
F 2 "capacitors:C_0603" H 8450 3850 50  0001 C CNN
F 3 "" H 8450 4000 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 8450 4000 50  0001 C CNN "manf#"
F 5 "1" H 8450 4000 50  0001 C CNN "check"
	1    8450 4000
	0    1    1    0   
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR075
U 1 1 5EDDA5C2
P 9850 4000
F 0 "#PWR075" H 9850 3750 50  0001 C CNN
F 1 "GND" H 9855 3827 50  0000 C CNN
F 2 "" H 9850 4000 50  0000 C CNN
F 3 "" H 9850 4000 50  0000 C CNN
	1    9850 4000
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:CONN_01X08-mechanical-connectors P6
U 1 1 5EBCA9E8
P 7700 5500
F 0 "P6" H 7778 5586 50  0000 L CNN
F 1 "CONN_01X08" H 7778 5495 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical_SMD_Pin1Left" H 7778 5404 50  0000 L CNN
F 3 "" H 7700 5500 50  0000 C CNN
	1    7700 5500
	1    0    0    -1  
$EndComp
Text GLabel 7500 5150 0    50   Input ~ 0
CS_DISPLAY
Text GLabel 7500 5450 0    50   Input ~ 0
SCK_0
Text GLabel 7500 5550 0    50   Input ~ 0
MOSI_0
$Comp
L driveshield-rescue:+3V3-power-supply #PWR070
U 1 1 5EC03F67
P 7050 5750
F 0 "#PWR070" H 7050 5600 50  0001 C CNN
F 1 "+3V3" H 7065 5923 50  0000 C CNN
F 2 "" H 7050 5750 50  0000 C CNN
F 3 "" H 7050 5750 50  0000 C CNN
	1    7050 5750
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR071
U 1 1 5EC04596
P 7150 5850
F 0 "#PWR071" H 7150 5600 50  0001 C CNN
F 1 "GND" H 7155 5677 50  0000 C CNN
F 2 "" H 7150 5850 50  0000 C CNN
F 3 "" H 7150 5850 50  0000 C CNN
	1    7150 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5750 7050 5750
Wire Wire Line
	7500 5850 7150 5850
Text GLabel 7400 3750 0    50   Output ~ 0
RST
Wire Wire Line
	9850 3750 9850 4000
Connection ~ 1000 950 
Text GLabel 3050 2250 2    50   Output ~ 0
PUSH_BTN_B
$Comp
L driveshield-rescue:SW_PUSH-devices SW1
U 1 1 5EA8E211
P 1850 950
F 0 "SW1" H 1850 1295 50  0000 C CNN
F 1 "NO" H 1850 1204 50  0000 C CNN
F 2 "mechanical-switches:smd_push" H 1850 1113 50  0000 C CNN
F 3 "" H 1850 950 50  0000 C CNN
F 4 "FSMJMLPTR" H 1850 950 50  0001 C CNN "manf#"
F 5 "1" H 1850 950 50  0001 C CNN "check"
	1    1850 950 
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C42
U 1 1 5EA914F0
P 3200 1250
F 0 "C42" V 2971 1250 50  0000 C CNN
F 1 "CAP CER 100nF 6V 0603" V 3062 1250 50  0000 C CNN
F 2 "capacitors:C_0603" H 3200 1100 50  0001 C CNN
F 3 "" H 3200 1250 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 3200 1250 50  0001 C CNN "manf#"
F 5 "1" H 3200 1250 50  0001 C CNN "check"
	1    3200 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	8800 3750 8850 3750
Wire Wire Line
	8350 4000 8150 4000
Wire Wire Line
	8150 4000 8150 3750
Connection ~ 8150 3750
Wire Wire Line
	8150 3750 8200 3750
Wire Wire Line
	8550 4000 8850 4000
Wire Wire Line
	8850 4000 8850 3750
Connection ~ 8850 3750
Wire Wire Line
	8850 3750 9850 3750
Wire Wire Line
	7850 3750 8150 3750
$Comp
L driveshield-rescue:+3V3-power-supply #PWR072
U 1 1 5EEF328A
P 7850 3550
F 0 "#PWR072" H 7850 3400 50  0001 C CNN
F 1 "+3V3" H 7865 3723 50  0000 C CNN
F 2 "" H 7850 3550 50  0000 C CNN
F 3 "" H 7850 3550 50  0000 C CNN
	1    7850 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3750 7400 3750
Connection ~ 7850 3750
Wire Wire Line
	3300 950  3400 950 
Wire Wire Line
	2600 2100 2600 1800
$Comp
L driveshield-rescue:R_0603-devices R56
U 1 1 5EFCE3BD
P 2850 2100
F 0 "R56" V 2654 2100 50  0000 C CNN
F 1 "RES 100 10% 0603" V 2745 2100 50  0000 C CNN
F 2 "resistors:R_0603" H 2850 1950 50  0001 C CNN
F 3 "" H 2850 2100 50  0000 C CNN
F 4 "RC0603FR-07100RL" H 2850 2100 50  0001 C CNN "manf#"
F 5 "1" H 2850 2100 50  0001 C CNN "check"
	1    2850 2100
	0    1    1    0   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R58
U 1 1 5EF52214
P 3250 1800
F 0 "R58" V 3054 1800 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 3145 1800 50  0000 C CNN
F 2 "resistors:R_0603" H 3250 1650 50  0001 C CNN
F 3 "" H 3250 1800 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 3250 1800 50  0001 C CNN "manf#"
F 5 "1" H 3250 1800 50  0001 C CNN "check"
	1    3250 1800
	0    1    1    0   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R57
U 1 1 5EE73FF6
P 3200 950
F 0 "R57" V 3396 950 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 3305 950 50  0000 C CNN
F 2 "resistors:R_0603" H 3200 800 50  0001 C CNN
F 3 "" H 3200 950 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 3200 950 50  0001 C CNN "manf#"
F 5 "1" H 3200 950 50  0001 C CNN "check"
	1    3200 950 
	0    -1   -1   0   
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R55
U 1 1 5F02748E
P 2850 1250
F 0 "R55" V 3046 1250 50  0000 C CNN
F 1 "RES 100 10% 0603" V 2955 1250 50  0000 C CNN
F 2 "resistors:R_0603" H 2850 1100 50  0001 C CNN
F 3 "" H 2850 1250 50  0000 C CNN
F 4 "RC0603FR-07100RL" H 2850 1250 50  0001 C CNN "manf#"
F 5 "1" H 2850 1250 50  0001 C CNN "check"
	1    2850 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 1250 3400 950 
Wire Wire Line
	2600 1250 2750 1250
Wire Wire Line
	3300 1250 3400 1250
Wire Wire Line
	2950 1250 3000 1250
Connection ~ 3400 950 
Wire Wire Line
	2150 950  2600 950 
Wire Wire Line
	1000 950  1550 950 
Wire Wire Line
	2600 950  2600 1250
Wire Wire Line
	3150 2100 3000 2100
Wire Wire Line
	2750 2100 2600 2100
Wire Wire Line
	2150 1800 2600 1800
Wire Wire Line
	1000 1800 1550 1800
Wire Wire Line
	1000 850  1000 950 
Wire Wire Line
	3700 950  3700 1800
Wire Wire Line
	3400 950  3700 950 
Wire Wire Line
	3350 2100 3700 2100
Connection ~ 3700 2100
Wire Wire Line
	3700 2100 3700 2250
Wire Wire Line
	3350 1800 3700 1800
Connection ~ 3700 1800
Wire Wire Line
	3700 1800 3700 2100
Wire Wire Line
	3050 2250 3000 2250
Wire Wire Line
	3000 2250 3000 2100
Connection ~ 3000 2100
Wire Wire Line
	3000 2100 2950 2100
Wire Wire Line
	3050 1400 3000 1400
Wire Wire Line
	3000 1400 3000 1250
Connection ~ 3000 1250
Wire Wire Line
	3000 1250 3100 1250
Wire Wire Line
	1000 950  1000 1800
$Comp
L driveshield-rescue:R_0603-devices R63
U 1 1 5F0D6EEE
P 5300 2900
F 0 "R63" V 5104 2900 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 5195 2900 50  0000 C CNN
F 2 "resistors:R_0603" H 5300 2750 50  0001 C CNN
F 3 "" H 5300 2900 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 5300 2900 50  0001 C CNN "manf#"
F 5 "1" H 5300 2900 50  0001 C CNN "check"
	1    5300 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2900 5600 2900
$Comp
L driveshield-rescue:R_0603-devices R64
U 1 1 5F1AE106
P 5300 3650
F 0 "R64" V 5104 3650 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 5195 3650 50  0000 C CNN
F 2 "resistors:R_0603" H 5300 3500 50  0001 C CNN
F 3 "" H 5300 3650 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 5300 3650 50  0001 C CNN "manf#"
F 5 "1" H 5300 3650 50  0001 C CNN "check"
	1    5300 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 3650 5600 3650
Connection ~ 5600 3650
$Comp
L driveshield-rescue:R_0603-devices R65
U 1 1 5F1C367C
P 5300 4400
F 0 "R65" V 5104 4400 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 5195 4400 50  0000 C CNN
F 2 "resistors:R_0603" H 5300 4250 50  0001 C CNN
F 3 "" H 5300 4400 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 5300 4400 50  0001 C CNN "manf#"
F 5 "1" H 5300 4400 50  0001 C CNN "check"
	1    5300 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 4400 5600 4400
Connection ~ 5600 4400
$Comp
L driveshield-rescue:R_0603-devices R66
U 1 1 5F1D8F14
P 5300 5150
F 0 "R66" V 5104 5150 50  0000 C CNN
F 1 "RES 10k 10% 0603" V 5195 5150 50  0000 C CNN
F 2 "resistors:R_0603" H 5300 5000 50  0001 C CNN
F 3 "" H 5300 5150 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 5300 5150 50  0001 C CNN "manf#"
F 5 "1" H 5300 5150 50  0001 C CNN "check"
	1    5300 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 5150 5600 5150
Connection ~ 5600 5150
$Comp
L driveshield-rescue:SW_DIP_x4-mechanical-switches S1
U 1 1 5EAB189E
P 3600 3900
F 0 "S1" H 3600 4365 50  0000 C CNN
F 1 "SW_DIP" H 3600 4250 50  0000 C CNN
F 2 "mechanical-switches:dip_4-300" H 3550 3900 50  0001 C CNN
F 3 "http://www.ctscorp.com/components/Datasheets/206-208.pdf" H 3600 4183 50  0001 C CNN
F 4 "208-4" H 3600 3900 50  0001 C CNN "manf#"
F 5 "1" H 3600 3900 50  0001 C CNN "check"
	1    3600 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3950 3950 4100
Wire Wire Line
	3950 3950 3900 3950
Wire Wire Line
	3900 3850 3950 3850
Connection ~ 5050 2900
Wire Wire Line
	5050 2900 5050 3050
Wire Wire Line
	3900 2900 3900 3750
Wire Wire Line
	3950 3850 3950 3650
Text Label 4700 3650 0    50   ~ 0
SW_2
$Comp
L driveshield-rescue:JUMPER3-devices JP4
U 1 1 5F2DCBEE
P 7050 1750
F 0 "JP4" H 7050 1989 50  0000 C CNN
F 1 "CONN_01X03" H 7050 1898 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7050 1897 50  0001 C CNN
F 3 "" H 7050 1750 50  0000 C CNN
	1    7050 1750
	1    0    0    -1  
$EndComp
Connection ~ 8000 1950
Wire Wire Line
	7050 1950 7050 1850
Wire Wire Line
	3100 3600 3100 3750
Wire Wire Line
	2100 3600 2100 3550
Wire Wire Line
	1800 3450 1850 3450
Text GLabel 6750 1750 0    50   Input ~ 0
SW2_EN
Wire Wire Line
	6800 1750 6750 1750
NoConn ~ 7300 1750
Wire Wire Line
	4600 4100 4750 4100
Wire Wire Line
	4750 4100 4750 4400
Wire Wire Line
	3900 4050 3900 5150
Wire Wire Line
	5200 5150 5050 5150
Wire Wire Line
	5050 3650 5200 3650
Wire Wire Line
	5050 2900 5200 2900
Wire Wire Line
	3000 2100 3000 1800
Wire Wire Line
	3000 1800 3150 1800
Wire Wire Line
	3000 1250 3000 950 
Wire Wire Line
	3000 950  3100 950 
Wire Wire Line
	5600 2900 5600 3650
Wire Wire Line
	5600 3650 5600 4400
Wire Wire Line
	5600 4400 5600 5150
Wire Wire Line
	5050 4400 5200 4400
Wire Wire Line
	5600 5150 5600 5200
Wire Wire Line
	4550 2900 5050 2900
Wire Wire Line
	4600 3650 5050 3650
Wire Wire Line
	4750 4400 5050 4400
Wire Wire Line
	4550 5150 5050 5150
Wire Wire Line
	2100 3600 3100 3600
$Comp
L driveshield-rescue:D_Small-devices D12
U 1 1 5F058225
P 9450 2050
F 0 "D12" H 9650 2000 50  0000 C CNN
F 1 "BAT46W" H 9200 2000 50  0000 C CNN
F 2 "diodes:SOD-123" H 9450 1937 50  0001 C CNN
F 3 "" V 9450 2050 50  0000 C CNN
F 4 "BAT46W-E3-08" H 9450 2050 50  0001 C CNN "manf#"
F 5 "1" H 9450 2050 50  0001 C CNN "check"
	1    9450 2050
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:D_Small-devices D13
U 1 1 5F05D4D7
P 9450 2150
F 0 "D13" H 9650 2100 50  0000 C CNN
F 1 "BAT46W" H 9200 2100 50  0000 C CNN
F 2 "diodes:SOD-123" H 9450 2037 50  0001 C CNN
F 3 "" V 9450 2150 50  0000 C CNN
F 4 "BAT46W-E3-08" H 9450 2150 50  0001 C CNN "manf#"
F 5 "1" H 9450 2150 50  0001 C CNN "check"
	1    9450 2150
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:D_Small-devices D14
U 1 1 5F062762
P 9450 2250
F 0 "D14" H 9650 2200 50  0000 C CNN
F 1 "BAT46W" H 9200 2200 50  0000 C CNN
F 2 "diodes:SOD-123" H 9450 2137 50  0001 C CNN
F 3 "" V 9450 2250 50  0000 C CNN
F 4 "BAT46W-E3-08" H 9450 2250 50  0001 C CNN "manf#"
F 5 "1" H 9450 2250 50  0001 C CNN "check"
	1    9450 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8950 1950 9350 1950
Wire Wire Line
	9350 2050 8950 2050
Wire Wire Line
	9350 2150 8950 2150
Wire Wire Line
	9350 2250 8950 2250
Wire Wire Line
	7050 1950 8000 1950
$Comp
L driveshield-rescue:D_Small-devices D7
U 1 1 5F090F02
P 4450 2900
F 0 "D7" H 4450 2605 50  0000 C CNN
F 1 "BAT46W" H 4450 2696 50  0000 C CNN
F 2 "diodes:SOD-123" H 4450 2787 50  0000 C CNN
F 3 "" V 4450 2900 50  0000 C CNN
F 4 "BAT46W-E3-08" H 4450 2900 50  0001 C CNN "manf#"
F 5 "1" H 4450 2900 50  0001 C CNN "check"
	1    4450 2900
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:D_Small-devices D9
U 1 1 5F0963DE
P 4500 3650
F 0 "D9" H 4500 3355 50  0000 C CNN
F 1 "BAT46W" H 4500 3446 50  0000 C CNN
F 2 "diodes:SOD-123" H 4500 3537 50  0000 C CNN
F 3 "" V 4500 3650 50  0000 C CNN
F 4 "BAT46W-E3-08" H 4500 3650 50  0001 C CNN "manf#"
F 5 "1" H 4500 3650 50  0001 C CNN "check"
	1    4500 3650
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:D_Small-devices D10
U 1 1 5F09B990
P 4500 4100
F 0 "D10" H 4500 3805 50  0000 C CNN
F 1 "BAT46W" H 4500 3896 50  0000 C CNN
F 2 "diodes:SOD-123" H 4500 3987 50  0000 C CNN
F 3 "" V 4500 4100 50  0000 C CNN
F 4 "BAT46W-E3-08" H 4500 4100 50  0001 C CNN "manf#"
F 5 "1" H 4500 4100 50  0001 C CNN "check"
	1    4500 4100
	-1   0    0    1   
$EndComp
$Comp
L driveshield-rescue:D_Small-devices D8
U 1 1 5F0A1166
P 4450 5150
F 0 "D8" H 4450 4855 50  0000 C CNN
F 1 "BAT46W" H 4450 4946 50  0000 C CNN
F 2 "diodes:SOD-123" H 4450 5037 50  0000 C CNN
F 3 "" V 4450 5150 50  0000 C CNN
F 4 "BAT46W-E3-08" H 4450 5150 50  0001 C CNN "manf#"
F 5 "1" H 4450 5150 50  0001 C CNN "check"
	1    4450 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 5150 4350 5150
Wire Wire Line
	4400 4100 3950 4100
Wire Wire Line
	3950 3650 4400 3650
Wire Wire Line
	3900 2900 4350 2900
$Comp
L driveshield-rescue:D_Small-devices D11
U 1 1 5F053017
P 9450 1950
F 0 "D11" H 9650 1900 50  0000 C CNN
F 1 "BAT46W" H 9200 1900 50  0000 C CNN
F 2 "diodes:SOD-123" H 9450 1837 50  0001 C CNN
F 3 "" V 9450 1950 50  0000 C CNN
F 4 "BAT46W-E3-08" H 9450 1950 50  0001 C CNN "manf#"
F 5 "1" H 9450 1950 50  0001 C CNN "check"
	1    9450 1950
	-1   0    0    1   
$EndComp
Wire Notes Line
	6100 2700 10700 2700
Wire Notes Line
	6200 4700 10550 4700
Text Notes 7200 1300 0    50   ~ 0
Option 1 second switch bank for removal bridge matrix diodes
Text Notes 7150 3150 0    50   ~ 0
Option 2 reset swit for removal omit
Text Notes 7100 4950 0    50   ~ 0
Option 3 Display header for removal omit with degraded spi lines
NoConn ~ 7450 5650
NoConn ~ 7450 5250
Wire Wire Line
	7450 5250 7500 5250
Wire Wire Line
	7500 5650 7450 5650
Text Notes 7850 5250 0    50   ~ 0
Pinout for Waveshare OLED Display WS9085
Wire Notes Line
	4750 5450 4750 2550
Wire Notes Line
	4750 2550 4100 2550
Wire Notes Line
	4100 2550 4100 5450
Wire Notes Line
	4100 5450 4750 5450
Text Notes 3850 5400 0    50   ~ 0
Matrix Diodes with Option 1
Wire Wire Line
	1850 5050 2800 5050
Text GLabel 7450 5350 0    50   Input ~ 0
DSP_CD
Wire Wire Line
	7500 5350 7450 5350
$EndSCHEMATC
