EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5200 3400 0    50   Output ~ 0
RX_1
Text GLabel 5200 3300 0    50   Input ~ 0
TX_1
$Comp
L driveshield-rescue:GND-power-supply #PWR086
U 1 1 5EB962B0
P 5850 3800
F 0 "#PWR086" H 5850 3550 50  0001 C CNN
F 1 "GND" H 5855 3627 50  0000 C CNN
F 2 "" H 5850 3800 50  0000 C CNN
F 3 "" H 5850 3800 50  0000 C CNN
	1    5850 3800
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:Jumper_NO_Small-devices JP3
U 1 1 5EB96F55
P 5400 3400
F 0 "JP3" H 5400 3350 50  0000 C CNN
F 1 "Jumper_solder" H 5410 3340 50  0001 C CNN
F 2 "wire_pads:SolderJumper_2mm" H 5400 3250 50  0000 C CNN
F 3 "" H 5400 3400 50  0000 C CNN
F 4 "1" H 5400 3400 50  0001 C CNN "DNP"
	1    5400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3400 5200 3400
$Comp
L driveshield-rescue:+3V3-power-supply #PWR085
U 1 1 5EC1C1C7
P 5650 2950
F 0 "#PWR085" H 5650 2800 50  0001 C CNN
F 1 "+3V3" H 5665 3123 50  0000 C CNN
F 2 "" H 5650 2950 50  0000 C CNN
F 3 "" H 5650 2950 50  0000 C CNN
	1    5650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3300 5200 3300
Wire Wire Line
	6050 3500 5850 3500
Wire Wire Line
	6050 3600 5650 3600
Wire Wire Line
	5650 2950 5650 3600
Wire Wire Line
	5850 3500 5850 3800
Wire Wire Line
	6050 3400 5500 3400
$Comp
L driveshield-rescue:CONN_01X06-mechanical-connectors P13
U 1 1 5F4199FB
P 6250 3450
F 0 "P13" H 6328 3536 50  0000 L CNN
F 1 "CONN_01X06" H 6328 3445 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 6328 3354 50  0000 L CNN
F 3 "" H 6250 3450 50  0000 C CNN
	1    6250 3450
	1    0    0    1   
$EndComp
Text Notes 4650 4200 0    50   ~ 0
Option 1 enable RX on bluetooth,\n shares pin with CAM_CLK 
NoConn ~ 6050 3200
NoConn ~ 6050 3700
$EndSCHEMATC
