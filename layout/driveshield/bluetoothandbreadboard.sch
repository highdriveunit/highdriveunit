EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FRDM-KL25Z:FRDM-KL25Z J?
U 1 1 5F6A510C
P 3650 2800
F 0 "J?" H 3350 3967 50  0000 C CNN
F 1 "FRDM-KL25Z" H 3350 3876 50  0000 C CNN
F 2 "FRDM_KL25Z:FRDM-KL25Z-header-top" H 3650 2800 50  0001 L BNN
F 3 "" H 3650 2800 50  0001 C CNN
	1    3650 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
