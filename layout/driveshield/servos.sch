EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L driveshield-rescue:OPA2374AIDCN-opamps U1
U 1 1 5EB2EC01
P 4300 2300
F 0 "U1" H 4844 2353 60  0000 L CNN
F 1 "OPA2374AIDCN LM358" H 4844 2247 60  0000 L CNN
F 2 "SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4300 2300 60  0001 C CNN
F 3 "" H 4300 2300 60  0000 C CNN
F 4 "LM358DR" H 4300 2300 50  0001 C CNN "manf#"
F 5 "" H 4300 2300 50  0001 C CNN "manf"
F 6 "Vfull" H 4300 2300 50  0001 C CNN "variant"
F 7 "1" H 4300 2300 50  0001 C CNN "check"
	1    4300 2300
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:CONN_01X03-mechanical-connectors P1
U 1 1 5EB308F7
P 7950 1850
F 0 "P1" H 8028 1936 50  0000 L CNN
F 1 "SERVO_0" H 8028 1845 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8028 1754 50  0000 L CNN
F 3 "" H 7950 1850 50  0000 C CNN
	1    7950 1850
	1    0    0    1   
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C1
U 1 1 5EB4E895
P 5850 2500
F 0 "C1" H 5942 2546 50  0000 L CNN
F 1 "CAP CER 1nF 6V 0603" H 5942 2455 50  0000 L CNN
F 2 "capacitors:C_0603" H 5850 2350 50  0001 C CNN
F 3 "" H 5850 2500 50  0000 C CNN
F 4 "CC0603KRX7R9BB102" H 5850 2500 50  0001 C CNN "manf#"
F 5 "Vfull" H 5850 2500 50  0001 C CNN "variant"
F 6 "1" H 5850 2500 50  0001 C CNN "check"
	1    5850 2500
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R1
U 1 1 5EB5B3FD
P 1850 3200
F 0 "R1" H 1909 3246 50  0000 L CNN
F 1 "RES 100k 10% 0603" H 1909 3155 50  0000 L CNN
F 2 "resistors:R_0603" H 1850 3050 50  0001 C CNN
F 3 "" H 1850 3200 50  0000 C CNN
F 4 "RR0816P-104-D" H 1850 3200 50  0001 C CNN "manf#"
F 5 "Vfull" H 1850 3200 50  0001 C CNN "variant"
F 6 "1" H 1850 3200 50  0001 C CNN "check"
	1    1850 3200
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:POT-devices RV1
U 1 1 5EB6144B
P 1850 3700
F 0 "RV1" V 1896 3622 50  0000 R CNN
F 1 "Potentiometer 1k 10% THT" V 1805 3622 50  0000 R CNN
F 2 "potentiometers:Potentiometer_Bourns_3296P_3-8Zoll_Angular_ScrewFront" H 1850 3700 50  0001 C CNN
F 3 "" H 1850 3700 50  0000 C CNN
F 4 "3299P-1-102LF" H 1850 3700 50  0001 C CNN "manf#"
F 5 "Vfull" H 1850 3700 50  0001 C CNN "variant"
F 6 "1" H 1850 3700 50  0001 C CNN "check"
	1    1850 3700
	0    1    -1   0   
$EndComp
$Comp
L driveshield-rescue:+6V-power-supply #PWR06
U 1 1 5EB69923
P 1850 2900
F 0 "#PWR06" H 1850 2750 50  0001 C CNN
F 1 "+6V" H 1865 3073 50  0000 C CNN
F 2 "" H 1850 2900 50  0000 C CNN
F 3 "" H 1850 2900 50  0000 C CNN
	1    1850 2900
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+6V-power-supply #PWR09
U 1 1 5EB69D6B
P 4200 1900
F 0 "#PWR09" H 4200 1750 50  0001 C CNN
F 1 "+6V" H 4215 2073 50  0000 C CNN
F 2 "" H 4200 1900 50  0000 C CNN
F 3 "" H 4200 1900 50  0000 C CNN
	1    4200 1900
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR07
U 1 1 5EB6A008
P 1850 4400
F 0 "#PWR07" H 1850 4150 50  0001 C CNN
F 1 "GND" H 1855 4227 50  0000 C CNN
F 2 "" H 1850 4400 50  0000 C CNN
F 3 "" H 1850 4400 50  0000 C CNN
	1    1850 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3100 1850 2900
Wire Wire Line
	1850 3450 1850 3400
Connection ~ 1850 3400
Wire Wire Line
	1850 3400 1850 3300
Text Label 2700 3700 0    50   ~ 0
VREF
Text Label 3300 2400 0    50   ~ 0
VREF
$Comp
L driveshield-rescue:+6V-power-supply #PWR012
U 1 1 5EB7450D
P 6300 1700
F 0 "#PWR012" H 6300 1550 50  0001 C CNN
F 1 "+6V" H 6315 1873 50  0000 C CNN
F 2 "" H 6300 1700 50  0000 C CNN
F 3 "" H 6300 1700 50  0000 C CNN
	1    6300 1700
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR08
U 1 1 5EB7770F
P 4200 2800
F 0 "#PWR08" H 4200 2550 50  0001 C CNN
F 1 "GND" H 4205 2627 50  0000 C CNN
F 2 "" H 4200 2800 50  0000 C CNN
F 3 "" H 4200 2800 50  0000 C CNN
	1    4200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2700 4200 2750
Wire Wire Line
	6300 2700 6300 2800
Wire Wire Line
	6300 2500 6300 2700
Connection ~ 6300 2700
Wire Wire Line
	6300 3000 6300 3200
Wire Wire Line
	5850 2600 5850 2700
Wire Wire Line
	7750 1950 7550 1950
Wire Wire Line
	6300 1950 6300 2100
Wire Wire Line
	6300 1700 6300 1850
Text GLabel 7400 1750 0    50   Input ~ 0
SERVO_0
Wire Wire Line
	7750 1750 7400 1750
$Comp
L driveshield-rescue:CONN_01X03-mechanical-connectors P2
U 1 1 5EB9A150
P 7950 4400
F 0 "P2" H 8028 4486 50  0000 L CNN
F 1 "SERVO_1" H 8028 4395 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8028 4304 50  0000 L CNN
F 3 "" H 7950 4400 50  0000 C CNN
	1    7950 4400
	1    0    0    1   
$EndComp
$Comp
L driveshield-rescue:R_2512-devices R9
U 1 1 5EB9A15C
P 6300 5450
F 0 "R9" H 6359 5496 50  0000 L CNN
F 1 "0.020 0.25% 2412" H 6359 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_2816_7142Metric" H 6300 5300 50  0001 C CIB
F 3 "" H 6300 5450 50  0000 C CNN
F 4 "LVK24R020CER" H 6300 5450 50  0001 C CNN "manf#"
F 5 "Vfull" H 6300 5450 50  0001 C CNN "variant"
F 6 "1" H 6300 5450 50  0001 C CNN "check"
	1    6300 5450
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C2
U 1 1 5EB9A168
P 5750 5050
F 0 "C2" H 5842 5096 50  0000 L CNN
F 1 "CAP CER 1nF 6V 0603" H 5842 5005 50  0000 L CNN
F 2 "capacitors:C_0603" H 5750 4900 50  0001 C CNN
F 3 "" H 5750 5050 50  0000 C CNN
F 4 "CC0603KRX7R9BB102" H 5750 5050 50  0001 C CNN "manf#"
F 5 "Vfull" H 5750 5050 50  0001 C CNN "variant"
F 6 "1" H 5750 5050 50  0001 C CNN "check"
	1    5750 5050
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+6V-power-supply #PWR011
U 1 1 5EB9A174
P 4200 4450
F 0 "#PWR011" H 4200 4300 50  0001 C CNN
F 1 "+6V" H 4215 4623 50  0000 C CNN
F 2 "" H 4200 4450 50  0000 C CNN
F 3 "" H 4200 4450 50  0000 C CNN
	1    4200 4450
	1    0    0    -1  
$EndComp
Text Label 3250 4950 0    50   ~ 0
VREF
$Comp
L driveshield-rescue:+6V-power-supply #PWR014
U 1 1 5EB9A184
P 6300 4250
F 0 "#PWR014" H 6300 4100 50  0001 C CNN
F 1 "+6V" H 6315 4423 50  0000 C CNN
F 2 "" H 6300 4250 50  0000 C CNN
F 3 "" H 6300 4250 50  0000 C CNN
	1    6300 4250
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR010
U 1 1 5EB9A18A
P 4200 5350
F 0 "#PWR010" H 4200 5100 50  0001 C CNN
F 1 "GND" H 4205 5177 50  0000 C CNN
F 2 "" H 4200 5350 50  0000 C CNN
F 3 "" H 4200 5350 50  0000 C CNN
	1    4200 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5250 4200 5350
Wire Wire Line
	6300 5250 6300 5350
Wire Wire Line
	6300 5050 6300 5250
Connection ~ 6300 5250
Wire Wire Line
	6300 5550 6300 5750
Wire Wire Line
	5750 5150 5750 5250
Wire Wire Line
	5750 4950 5750 4850
Wire Wire Line
	7750 4500 7550 4500
Wire Wire Line
	6300 4500 6300 4650
Wire Wire Line
	6300 4250 6300 4400
Text GLabel 7400 4300 0    50   Input ~ 0
SERVO_1
Wire Wire Line
	7750 4300 7400 4300
$Comp
L driveshield-rescue:OPA2374AIDCN-opamps U1
U 2 1 5EB9D39F
P 4300 4850
F 0 "U1" H 4844 4903 60  0000 L CNN
F 1 "OPA2374AIDCN LM358" H 4844 4797 60  0000 L CNN
F 2 "SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 4300 4850 60  0001 C CNN
F 3 "" H 4300 4850 60  0000 C CNN
F 4 "LM358DR" H 4300 4850 50  0001 C CNN "manf#"
F 5 "" H 4300 4850 50  0001 C CNN "manf"
F 6 "Vfull" H 4300 4850 50  0001 C CNN "variant"
F 7 "1" H 4300 4850 50  0001 C CNN "check"
	2    4300 4850
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:Q_NMOS_GDS-devices Q1
U 1 1 5EB0955A
P 6200 2300
F 0 "Q1" H 6391 2391 50  0000 L CNN
F 1 "DMN6068" H 6391 2300 50  0000 L CNN
F 2 "SOT_TO:TO-252-2Lead" H 6391 2209 50  0000 L CNN
F 3 "" H 6200 2300 50  0000 C CNN
F 4 "DMN6068LK3-13" H 6200 2300 50  0001 C CNN "manf#"
F 5 "Vfull" H 6200 2300 50  0001 C CNN "variant"
F 6 "1" H 6200 2300 50  0001 C CNN "check"
	1    6200 2300
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:Q_NMOS_GDS-devices Q2
U 1 1 5EB0F3C4
P 6200 4850
F 0 "Q2" H 6391 4941 50  0000 L CNN
F 1 "DMN6068" H 6391 4850 50  0000 L CNN
F 2 "SOT_TO:TO-252-2Lead" H 6391 4759 50  0000 L CNN
F 3 "" H 6200 4850 50  0000 C CNN
F 4 "DMN6068LK3-13" H 6200 4850 50  0001 C CNN "manf#"
F 5 "Vfull" H 6200 4850 50  0001 C CNN "variant"
F 6 "1" H 6200 4850 50  0001 C CNN "check"
	1    6200 4850
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR013
U 1 1 5EB07840
P 6300 3200
F 0 "#PWR013" H 6300 2950 50  0001 C CNN
F 1 "GND" H 6305 3027 50  0000 C CNN
F 2 "" H 6300 3200 50  0000 C CNN
F 3 "" H 6300 3200 50  0000 C CNN
	1    6300 3200
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR015
U 1 1 5EB08C8A
P 6300 5750
F 0 "#PWR015" H 6300 5500 50  0001 C CNN
F 1 "GND" H 6305 5577 50  0000 C CNN
F 2 "" H 6300 5750 50  0000 C CNN
F 3 "" H 6300 5750 50  0000 C CNN
	1    6300 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3400 2150 3400
$Comp
L driveshield-rescue:R_2512-devices R8
U 1 1 5EB3CCCA
P 6300 2900
F 0 "R8" H 6359 2946 50  0000 L CNN
F 1 "0.020 0.25% 2412" H 6359 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_2816_7142Metric" H 6300 2750 50  0001 C CIB
F 3 "" H 6300 2900 50  0000 C CNN
F 4 "LVK24R020CER" H 6300 2900 50  0001 C CNN "manf#"
F 5 "Vfull" H 6300 2900 50  0001 C CNN "variant"
F 6 "1" H 6300 2900 50  0001 C CNN "check"
	1    6300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3950 1850 4000
$Comp
L driveshield-rescue:R_0805-devices R2
U 1 1 5F40E676
P 2150 3850
F 0 "R2" H 2209 3896 50  0000 L CNN
F 1 "RES 470 10% 0603" H 2209 3805 50  0000 L CNN
F 2 "resistors:R_0603" H 2150 3700 50  0001 C CNN
F 3 "" H 2150 3850 50  0000 C CNN
F 4 "1" H 2150 3850 50  0001 C CNN "DNP"
	1    2150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3950 2150 4000
Wire Wire Line
	2150 4000 1850 4000
Connection ~ 1850 4000
Wire Wire Line
	1850 4000 1850 4400
Wire Wire Line
	2000 3700 2150 3700
Wire Wire Line
	2150 3750 2150 3700
Connection ~ 2150 3700
Wire Wire Line
	2150 3700 2900 3700
$Comp
L driveshield-rescue:Jumper_NO_Small-devices JP?
U 1 1 5F0DC568
P 7550 2750
AR Path="/5F6B3792/5F0DC568" Ref="JP?"  Part="1" 
AR Path="/5EB2E969/5F0DC568" Ref="JP5"  Part="1" 
F 0 "JP5" H 7550 2700 50  0000 C CNN
F 1 "Jumper_solder" H 7560 2690 50  0001 C CNN
F 2 "wire_pads:SolderJumper_3mm" H 7550 2600 50  0001 C CNN
F 3 "" H 7550 2750 50  0000 C CNN
F 4 "1" H 7550 2750 50  0001 C CNN "DNP"
	1    7550 2750
	0    -1   -1   0   
$EndComp
$Comp
L driveshield-rescue:Jumper_NO_Small-devices JP?
U 1 1 5F0DD35A
P 7550 5400
AR Path="/5F6B3792/5F0DD35A" Ref="JP?"  Part="1" 
AR Path="/5EB2E969/5F0DD35A" Ref="JP6"  Part="1" 
F 0 "JP6" H 7550 5350 50  0000 C CNN
F 1 "Jumper_solder" H 7560 5340 50  0001 C CNN
F 2 "wire_pads:SolderJumper_3mm" H 7550 5250 50  0000 C CNN
F 3 "" H 7550 5400 50  0000 C CNN
F 4 "1" H 7550 5400 50  0001 C CNN "DNP"
	1    7550 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 5300 7550 4500
Wire Wire Line
	7550 2650 7550 1950
Wire Wire Line
	7550 2850 7550 3050
$Comp
L driveshield-rescue:GND-power-supply #PWR0104
U 1 1 5F0E2F0A
P 7550 3050
F 0 "#PWR0104" H 7550 2800 50  0001 C CNN
F 1 "GND" H 7555 2877 50  0000 C CNN
F 2 "" H 7550 3050 50  0000 C CNN
F 3 "" H 7550 3050 50  0000 C CNN
	1    7550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 5500 7550 5700
$Comp
L driveshield-rescue:GND-power-supply #PWR0105
U 1 1 5F0E43EC
P 7550 5700
F 0 "#PWR0105" H 7550 5450 50  0001 C CNN
F 1 "GND" H 7555 5527 50  0000 C CNN
F 2 "" H 7550 5700 50  0000 C CNN
F 3 "" H 7550 5700 50  0000 C CNN
	1    7550 5700
	1    0    0    -1  
$EndComp
Connection ~ 7550 1950
Wire Wire Line
	7550 1950 6300 1950
Wire Wire Line
	6300 1850 7750 1850
Connection ~ 7550 4500
Wire Wire Line
	7550 4500 6300 4500
Wire Wire Line
	6300 4400 7750 4400
$Comp
L driveshield-rescue:C_0603-devices C44
U 1 1 5F097A14
P 2800 2250
F 0 "C44" H 2892 2296 50  0000 L CNN
F 1 "CAP CER 100nF 6V 0603" H 2892 2205 50  0000 L CNN
F 2 "capacitors:C_0603" H 2800 2100 50  0001 C CNN
F 3 "" H 2800 2250 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 2800 2250 50  0001 C CNN "manf#"
F 5 "Vfull" H 2800 2250 50  0001 C CNN "variant"
F 6 "1" H 2800 2250 50  0001 C CNN "check"
	1    2800 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1900 2800 1900
Wire Wire Line
	2800 2750 4200 2750
Wire Wire Line
	2800 1900 2800 2150
Wire Wire Line
	2800 2350 2800 2750
Text Notes 2350 1300 0    50   ~ 0
Option current limiting per channel, to remove omit and bridge jumpers in this sheet
Text Notes 1900 4300 0    50   ~ 0
optional placement of fixed resistors \nshown here for limit of aprox. 1 A per channel\npotentiometer gives a range from 2.2 A to 0A
Wire Wire Line
	4750 2700 4750 2000
Wire Wire Line
	3800 2000 3800 2200
Wire Wire Line
	3800 2000 4750 2000
Wire Wire Line
	3300 2400 3800 2400
Connection ~ 4200 1900
Connection ~ 4200 2750
Wire Wire Line
	4200 2750 4200 2800
Wire Wire Line
	3250 4950 3800 4950
Wire Wire Line
	4700 5250 4700 4500
Wire Wire Line
	4700 4500 3800 4500
Wire Wire Line
	3800 4500 3800 4750
Connection ~ 5750 4850
Wire Wire Line
	5750 4850 6000 4850
Connection ~ 5750 5250
Wire Wire Line
	5750 5250 6300 5250
Wire Wire Line
	4800 4850 5750 4850
Wire Wire Line
	4700 5250 5750 5250
Connection ~ 5850 2700
Wire Wire Line
	5850 2700 6300 2700
Wire Wire Line
	4750 2700 5850 2700
Wire Wire Line
	5850 2400 5850 2300
Connection ~ 5850 2300
Wire Wire Line
	5850 2300 6000 2300
Wire Wire Line
	4800 2300 5850 2300
Wire Wire Line
	2150 3400 2150 3700
Text Notes 4800 2100 0    50   ~ 0
opamp output current unlimited,\nbecause this model is self limiting
$EndSCHEMATC
