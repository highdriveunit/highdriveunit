EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L driveshield-rescue:MHP_3.2_5.0-MECH_mounting-holes MECH1
U 1 1 5EB615D8
P 4100 3000
F 0 "MECH1" H 4228 3046 50  0000 L CNN
F 1 "MHP_3.2_5.0" H 4228 2955 50  0000 L CNN
F 2 "MECH_mounting_holes:MHP_3.2_5.0" H 4000 3050 50  0001 C CNN
F 3 "" H 4100 3000 50  0001 C CNN
F 4 "1" H 4100 3000 50  0001 C CNN "DNP"
	1    4100 3000
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:MHP_3.2_5.0-MECH_mounting-holes MECH2
U 1 1 5EB6211F
P 4100 3250
F 0 "MECH2" H 4228 3296 50  0000 L CNN
F 1 "MHP_3.2_5.0" H 4228 3205 50  0000 L CNN
F 2 "MECH_mounting_holes:MHP_3.2_5.0" H 4000 3300 50  0001 C CNN
F 3 "" H 4100 3250 50  0001 C CNN
F 4 "1" H 4100 3250 50  0001 C CNN "DNP"
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:MHP_3.2_5.0-MECH_mounting-holes MECH3
U 1 1 5EB62259
P 4100 3500
F 0 "MECH3" H 4228 3546 50  0000 L CNN
F 1 "MHP_3.2_5.0" H 4228 3455 50  0000 L CNN
F 2 "MECH_mounting_holes:MHP_3.2_5.0" H 4000 3550 50  0001 C CNN
F 3 "" H 4100 3500 50  0001 C CNN
F 4 "1" H 4100 3500 50  0001 C CNN "DNP"
	1    4100 3500
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:MHP_3.2_5.0-MECH_mounting-holes MECH4
U 1 1 5EB6246E
P 4100 3750
F 0 "MECH4" H 4228 3796 50  0000 L CNN
F 1 "MHP_3.2_5.0" H 4228 3705 50  0000 L CNN
F 2 "MECH_mounting_holes:MHP_3.2_5.0" H 4000 3800 50  0001 C CNN
F 3 "" H 4100 3750 50  0001 C CNN
F 4 "1" H 4100 3750 50  0001 C CNN "DNP"
	1    4100 3750
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR084
U 1 1 5EEA47A0
P 3800 3950
F 0 "#PWR084" H 3800 3700 50  0001 C CNN
F 1 "GND" H 3805 3777 50  0000 C CNN
F 2 "" H 3800 3950 50  0000 C CNN
F 3 "" H 3800 3950 50  0000 C CNN
	1    3800 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3950 3800 3750
Connection ~ 3800 3750
Wire Wire Line
	3800 3750 3800 3500
$Comp
L driveshield-rescue:CONN_01X01-mechanical-connectors P14
U 1 1 5EFB1392
P 6750 3900
F 0 "P14" H 6828 3986 50  0000 L CNN
F 1 "Pad_Solder" H 6828 3895 50  0000 L CNN
F 2 "wire_pads:area2x2mm" H 6828 3804 50  0000 L CNN
F 3 "" H 6750 3900 50  0000 C CNN
F 4 "1" H 6750 3900 50  0001 C CNN "DNP"
	1    6750 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3900 6500 3900
Wire Wire Line
	6500 3900 6500 4100
$Comp
L driveshield-rescue:GND-power-supply #PWR0124
U 1 1 5EFB1AF3
P 6500 4100
F 0 "#PWR0124" H 6500 3850 50  0001 C CNN
F 1 "GND" H 6505 3927 50  0000 C CNN
F 2 "" H 6500 4100 50  0000 C CNN
F 3 "" H 6500 4100 50  0000 C CNN
	1    6500 4100
	1    0    0    -1  
$EndComp
Text Notes 6750 4150 0    50   ~ 0
For HW debug purposes\n
Text Notes 4000 4050 0    50   ~ 0
Grounded mounting holes come with risk, but such is life.
Connection ~ 3800 3250
Connection ~ 3800 3500
Wire Wire Line
	3800 3500 3800 3250
Wire Wire Line
	3800 3000 3800 3250
$EndSCHEMATC
