EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2550 2800 0    50   Input ~ 0
PUSH_BTN_A
Text GLabel 2550 3000 0    50   Input ~ 0
PUSH_BTN_B
Text GLabel 2550 4300 0    50   Input ~ 0
DIP_SW_1
Text GLabel 2550 4400 0    50   Input ~ 0
DIP_SW_2
Text GLabel 2550 4500 0    50   Input ~ 0
DIP_SW_3
Text GLabel 2550 4600 0    50   Input ~ 0
DIP_SW_4
Text GLabel 4950 2800 2    50   Output ~ 0
LED_1
Text GLabel 4950 2900 2    50   Output ~ 0
LED_2
Text GLabel 4950 3000 2    50   Output ~ 0
LED_3
Text GLabel 4950 3100 2    50   Output ~ 0
LED_4
Text GLabel 4950 2600 2    50   Input ~ 0
POT_B
Text GLabel 4950 2700 2    50   Input ~ 0
POT_A
Text GLabel 5000 4400 2    50   Input ~ 0
RST
$Comp
L driveshield-rescue:GND-power-supply #PWR02
U 1 1 5EA8454D
P 5050 3800
F 0 "#PWR02" H 5050 3550 50  0001 C CNN
F 1 "GND" H 5055 3627 50  0000 C CNN
F 2 "" H 5050 3800 50  0000 C CNN
F 3 "" H 5050 3800 50  0000 C CNN
	1    5050 3800
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR03
U 1 1 5EA850E6
P 5300 4000
F 0 "#PWR03" H 5300 3850 50  0001 C CNN
F 1 "+3V3" H 5315 4173 50  0000 C CNN
F 2 "" H 5300 4000 50  0000 C CNN
F 3 "" H 5300 4000 50  0000 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
Text GLabel 5600 1500 2    50   Input ~ 0
SENSOR_0
Text GLabel 5600 1600 2    50   Input ~ 0
SENSOR_1
Text GLabel 1450 4100 0    50   Output ~ 0
CAM_CLK
Text GLabel 2550 3900 0    50   Output ~ 0
CAM_SYNC
Text GLabel 2550 3700 0    50   Input ~ 0
CAM0_AI
Text GLabel 2550 3800 0    50   Input ~ 0
CAM1_AI
Text GLabel 4950 2400 2    50   Output ~ 0
SERVO_0
Text GLabel 4950 2500 2    50   Output ~ 0
SERVO_1
Text GLabel 2500 1800 0    50   Output ~ 0
MOTOR_A1
Text GLabel 2500 1900 0    50   Output ~ 0
MOTOR_A2
Text GLabel 2500 1600 0    50   Output ~ 0
MOTOR_B1
Text GLabel 2500 1700 0    50   Output ~ 0
MOTOR_B2
Text GLabel 1050 4650 0    50   Output ~ 0
MOTORS_ENBL
Text GLabel 2550 4700 0    50   Input ~ 0
MOTORS_FAULT
Text GLabel 2450 5550 0    50   Input ~ 0
VBAT_SENS
Text GLabel 2500 2400 0    50   BiDi ~ 0
SDA_0
Text GLabel 2500 2300 0    50   Output ~ 0
SCL_0
Text GLabel 2500 3300 0    50   Output ~ 0
SCK_0
Text GLabel 2500 3400 0    50   Output ~ 0
MOSI_0
Text GLabel 2500 3500 0    50   Input ~ 0
MISO_0
Text GLabel 1950 4200 0    50   Output ~ 0
TX_1
Text GLabel 1450 3950 0    50   Input ~ 0
RX_1
Wire Wire Line
	1450 3950 1850 3950
Wire Wire Line
	1850 3950 1850 4100
Connection ~ 1850 4100
Wire Wire Line
	1850 4100 1450 4100
Text Notes 1150 4000 2    50   ~ 0
UART_1\n(Bluetooth)
$Sheet
S 4700 6950 900  700 
U 5EB2E969
F0 "servos" 50
F1 "servos.sch" 50
$EndSheet
$Sheet
S 3450 6950 1000 700 
U 5EAB593B
F0 "powertrain" 50
F1 "powertrain.sch" 50
$EndSheet
$Sheet
S 2100 6950 1000 700 
U 5EAA17D5
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 650  6950 1050 700 
U 5EA7C97D
F0 "HMI" 50
F1 "HMI.sch" 50
$EndSheet
$Sheet
S 650  5850 1050 850 
U 5F6759FE
F0 "sensors" 50
F1 "sensors.sch" 50
$EndSheet
$Sheet
S 1950 5850 1150 850 
U 5F6A4F39
F0 "breakout" 50
F1 "breakout.sch" 50
$EndSheet
$Sheet
S 3300 5850 1100 850 
U 5F6B3792
F0 "bluetooth" 50
F1 "bluetooth.sch" 50
$EndSheet
Wire Bus Line
	4900 900  2600 900 
Entry Wire Line
	4900 4400 4800 4500
Entry Wire Line
	4900 4300 4800 4400
Entry Wire Line
	4900 4200 4800 4300
Entry Wire Line
	4900 4100 4800 4200
Entry Wire Line
	4900 4000 4800 4100
Entry Wire Line
	4900 3900 4800 4000
Entry Wire Line
	4900 3800 4800 3900
Entry Wire Line
	4900 3000 4800 3100
Entry Wire Line
	4900 2900 4800 3000
Entry Wire Line
	4900 2800 4800 2900
Entry Wire Line
	4900 2700 4800 2800
Entry Wire Line
	4900 2600 4800 2700
Entry Wire Line
	4900 2500 4800 2600
Entry Wire Line
	4900 2400 4800 2500
Entry Wire Line
	4900 2300 4800 2400
Entry Wire Line
	4900 2100 4800 2200
Entry Wire Line
	4900 2000 4800 2100
Entry Wire Line
	4900 1900 4800 2000
Entry Wire Line
	4900 1800 4800 1900
Entry Wire Line
	4900 1700 4800 1800
Entry Wire Line
	4900 1600 4800 1700
Entry Wire Line
	4900 1500 4800 1600
Entry Wire Line
	4900 1400 4800 1500
Entry Wire Line
	2600 1400 2700 1500
Entry Wire Line
	2600 1500 2700 1600
Entry Wire Line
	2600 1600 2700 1700
Entry Wire Line
	2600 1700 2700 1800
Entry Wire Line
	2600 1800 2700 1900
Entry Wire Line
	2600 1900 2700 2000
Entry Wire Line
	2600 2000 2700 2100
Entry Wire Line
	2600 2100 2700 2200
Entry Wire Line
	2600 2200 2700 2300
Entry Wire Line
	2600 2300 2700 2400
Entry Wire Line
	2600 2400 2700 2500
Entry Wire Line
	2600 2500 2700 2600
Entry Wire Line
	2600 2600 2700 2700
Entry Wire Line
	2600 2700 2700 2800
Entry Wire Line
	2600 2800 2700 2900
Entry Wire Line
	2600 2900 2700 3000
Entry Wire Line
	2600 3100 2700 3200
Entry Wire Line
	2600 3200 2700 3300
Entry Wire Line
	2600 3300 2700 3400
Entry Wire Line
	2600 3400 2700 3500
Entry Wire Line
	2600 3500 2700 3600
Entry Wire Line
	2600 3600 2700 3700
Entry Wire Line
	2600 3700 2700 3800
Entry Wire Line
	2600 3800 2700 3900
Entry Wire Line
	2600 4000 2700 4100
Entry Wire Line
	2600 4100 2700 4200
Entry Wire Line
	2600 4200 2700 4300
Entry Wire Line
	2600 4300 2700 4400
Entry Wire Line
	2600 4400 2700 4500
Entry Wire Line
	2600 4500 2700 4600
Entry Wire Line
	2600 4600 2700 4700
Entry Wire Line
	2600 4700 2700 4800
Entry Wire Line
	2600 4800 2700 4900
Entry Wire Line
	2600 4900 2700 5000
Entry Wire Line
	2600 5000 2700 5100
Entry Wire Line
	2600 5100 2700 5200
Wire Wire Line
	4650 4500 4800 4500
Wire Wire Line
	4650 4300 4800 4300
Wire Wire Line
	4650 4100 4800 4100
Wire Wire Line
	4650 2000 4800 2000
Wire Wire Line
	4650 1900 4800 1900
Wire Wire Line
	2700 3600 2850 3600
Wire Wire Line
	2850 2600 2700 2600
Wire Wire Line
	2850 2500 2700 2500
Wire Wire Line
	2850 2000 2700 2000
Wire Wire Line
	2850 2100 2700 2100
Wire Wire Line
	2850 2200 2700 2200
Wire Bus Line
	4900 4850 5100 4850
Text Label 4650 3900 0    50   ~ 0
GND
Text Label 2700 1600 0    50   ~ 0
C1
Text Label 2700 1700 0    50   ~ 0
C2
Text Label 2700 1800 0    50   ~ 0
C3
Text Label 2700 1900 0    50   ~ 0
C4
Text Label 2700 2000 0    50   ~ 0
C5
Text Label 2700 2100 0    50   ~ 0
C6
Text Label 2700 2200 0    50   ~ 0
C7
Text Label 2700 2300 0    50   ~ 0
C8
Text Label 2700 2400 0    50   ~ 0
C9
Text Label 2700 2500 0    50   ~ 0
C10
Text Label 2700 2600 0    50   ~ 0
C11
Text Label 2700 2700 0    50   ~ 0
C12
Text Label 2700 2800 0    50   ~ 0
C13
Text Label 2700 2900 0    50   ~ 0
C16
Text Label 2700 3000 0    50   ~ 0
C17
Text Label 2700 3200 0    50   ~ 0
D0
Text Label 2700 3300 0    50   ~ 0
D1
Text Label 2700 3400 0    50   ~ 0
D2
Text Label 2700 3500 0    50   ~ 0
D3
Text Label 2700 3600 0    50   ~ 0
D4
Text Label 2700 3700 0    50   ~ 0
D5
Text Label 2700 3800 0    50   ~ 0
D6
Text Label 2700 3900 0    50   ~ 0
D7
Text Label 2700 4100 0    50   ~ 0
E0
Text Label 2700 4200 0    50   ~ 0
E1
Text Label 2700 4300 0    50   ~ 0
E2
Text Label 2700 4400 0    50   ~ 0
E3
Text Label 2700 4500 0    50   ~ 0
E4
Text Label 2700 4600 0    50   ~ 0
E5
Text Label 2700 4700 0    50   ~ 0
E20
Text Label 2700 4800 0    50   ~ 0
E21
Text Label 2700 4900 0    50   ~ 0
E22
Text Label 2700 5000 0    50   ~ 0
E23
Text Label 2700 5100 0    50   ~ 0
E29
Text Label 2700 5200 0    50   ~ 0
E30
Text Label 2700 5300 0    50   ~ 0
E31
Text Label 4650 4500 0    50   ~ 0
SDA
Text Label 4650 4400 0    50   ~ 0
RES
Text Label 4650 4300 0    50   ~ 0
5V
Text Label 4650 4200 0    50   ~ 0
VIN
Text Label 4650 4100 0    50   ~ 0
REF
Text Label 4650 4000 0    50   ~ 0
3V3
Text Label 4650 3100 0    50   ~ 0
B11
Text Label 4650 3000 0    50   ~ 0
B10
Text Label 4650 2900 0    50   ~ 0
B9
Text Label 4650 2800 0    50   ~ 0
B8
Text Label 4650 2700 0    50   ~ 0
B3
Text Label 4650 2600 0    50   ~ 0
B2
Text Label 4650 2500 0    50   ~ 0
B1
Text Label 4650 2400 0    50   ~ 0
B0
Text Label 4650 2200 0    50   ~ 0
A17
Text Label 4650 2100 0    50   ~ 0
A16
Text Label 4650 2000 0    50   ~ 0
A13
Text Label 4650 1900 0    50   ~ 0
A12
Text Label 4650 1800 0    50   ~ 0
A5
Text Label 4650 1700 0    50   ~ 0
A4
Text Label 4650 1600 0    50   ~ 0
A2
Text Label 4650 1500 0    50   ~ 0
A1
Text Label 2700 1500 0    50   ~ 0
C0
Text GLabel 5600 2200 2    50   Output ~ 0
SW1_EN
Text Label 7800 1550 0    50   ~ 0
C0
Text Label 9750 1550 0    50   ~ 0
A1
Wire Wire Line
	7800 5150 7950 5150
Wire Wire Line
	7800 5050 7950 5050
Wire Wire Line
	7800 4950 7950 4950
Wire Wire Line
	7800 4850 7950 4850
Wire Wire Line
	7800 4750 7950 4750
Wire Wire Line
	7800 4650 7950 4650
Wire Wire Line
	7800 4550 7950 4550
Wire Wire Line
	7800 4450 7950 4450
Wire Wire Line
	7800 4350 7950 4350
Wire Wire Line
	7800 4250 7950 4250
Wire Wire Line
	7800 4150 7950 4150
Wire Wire Line
	7800 3950 7950 3950
Wire Wire Line
	7800 3850 7950 3850
Wire Wire Line
	7800 3750 7950 3750
Wire Wire Line
	7950 3550 7800 3550
Wire Wire Line
	7800 3450 7950 3450
Wire Wire Line
	7800 3350 7950 3350
Wire Wire Line
	7950 3050 7800 3050
Wire Wire Line
	7950 2850 7800 2850
Wire Wire Line
	7950 2450 7800 2450
Wire Wire Line
	7950 2350 7800 2350
Wire Wire Line
	7800 1950 7950 1950
Wire Wire Line
	7950 1850 7800 1850
Wire Wire Line
	7800 1750 7950 1750
Wire Wire Line
	7800 1650 7950 1650
Wire Wire Line
	9900 1550 9750 1550
Wire Wire Line
	9900 1650 9750 1650
Wire Wire Line
	9900 2450 9750 2450
Wire Wire Line
	9900 2550 9750 2550
Wire Wire Line
	9900 2650 9750 2650
Wire Wire Line
	9900 2750 9750 2750
Wire Wire Line
	9900 2850 9750 2850
Wire Wire Line
	9900 2950 9750 2950
Wire Wire Line
	9900 3050 9750 3050
Wire Wire Line
	9900 3150 9750 3150
Wire Wire Line
	9900 3950 9750 3950
Wire Wire Line
	9900 4050 9750 4050
Wire Wire Line
	9900 4250 9750 4250
Wire Wire Line
	9900 4450 9750 4450
Text Label 9750 1650 0    50   ~ 0
A2
Text Label 9750 1750 0    50   ~ 0
A4
Text Label 9750 1850 0    50   ~ 0
A5
Text Label 9750 1950 0    50   ~ 0
A12
Text Label 9750 2050 0    50   ~ 0
A13
Text Label 9750 2150 0    50   ~ 0
A16
Text Label 9750 2250 0    50   ~ 0
A17
Text Label 9750 2450 0    50   ~ 0
B0
Text Label 9750 2550 0    50   ~ 0
B1
Text Label 9750 2650 0    50   ~ 0
B2
Text Label 9750 2750 0    50   ~ 0
B3
Text Label 9750 2850 0    50   ~ 0
B8
Text Label 9750 2950 0    50   ~ 0
B9
Text Label 9750 3050 0    50   ~ 0
B10
Text Label 9750 3150 0    50   ~ 0
B11
Text Label 9750 4050 0    50   ~ 0
3V3
Text Label 9750 4150 0    50   ~ 0
REF
Text Label 9750 4250 0    50   ~ 0
VIN
Text Label 9750 4350 0    50   ~ 0
5V
Text Label 9750 4450 0    50   ~ 0
RES
Text Label 9750 4550 0    50   ~ 0
SDA
Text Label 7800 5350 0    50   ~ 0
E31
Text Label 7800 5250 0    50   ~ 0
E30
Text Label 7800 5150 0    50   ~ 0
E29
Text Label 7800 5050 0    50   ~ 0
E23
Text Label 7800 4950 0    50   ~ 0
E22
Text Label 7800 4850 0    50   ~ 0
E21
Text Label 7800 4750 0    50   ~ 0
E20
Text Label 7800 4650 0    50   ~ 0
E5
Text Label 7800 4550 0    50   ~ 0
E4
Text Label 7800 4450 0    50   ~ 0
E3
Text Label 7800 4350 0    50   ~ 0
E2
Text Label 7800 4250 0    50   ~ 0
E1
Text Label 7800 4150 0    50   ~ 0
E0
Text Label 7800 3950 0    50   ~ 0
D7
Text Label 7800 3850 0    50   ~ 0
D6
Text Label 7800 3750 0    50   ~ 0
D5
Text Label 7800 3650 0    50   ~ 0
D4
Text Label 7800 3550 0    50   ~ 0
D3
Text Label 7800 3450 0    50   ~ 0
D2
Text Label 7800 3350 0    50   ~ 0
D1
Text Label 7800 3250 0    50   ~ 0
D0
Text Label 7800 3050 0    50   ~ 0
C17
Text Label 7800 2950 0    50   ~ 0
C16
Text Label 7800 2850 0    50   ~ 0
C13
Text Label 7800 2750 0    50   ~ 0
C12
Text Label 7800 2650 0    50   ~ 0
C11
Text Label 7800 2550 0    50   ~ 0
C10
Text Label 7800 2450 0    50   ~ 0
C9
Text Label 7800 2350 0    50   ~ 0
C8
Text Label 7800 2250 0    50   ~ 0
C7
Text Label 7800 2150 0    50   ~ 0
C6
Text Label 7800 2050 0    50   ~ 0
C5
Text Label 7800 1950 0    50   ~ 0
C4
Text Label 7800 1850 0    50   ~ 0
C3
Text Label 7800 1750 0    50   ~ 0
C2
Text Label 7800 1650 0    50   ~ 0
C1
Text Label 9750 3950 0    50   ~ 0
GND
Wire Bus Line
	10000 4900 10200 4900
Wire Wire Line
	7950 1550 7800 1550
Wire Wire Line
	7950 2250 7800 2250
Wire Wire Line
	7950 2150 7800 2150
Wire Wire Line
	7950 2050 7800 2050
Wire Wire Line
	7950 2550 7800 2550
Wire Wire Line
	7950 2650 7800 2650
Wire Wire Line
	7800 2750 7950 2750
Wire Wire Line
	7800 2950 7950 2950
Wire Wire Line
	7800 3250 7950 3250
Wire Wire Line
	7800 3650 7950 3650
Wire Wire Line
	7800 5250 7950 5250
Wire Wire Line
	7950 5350 7800 5350
Wire Wire Line
	9750 1750 9900 1750
Wire Wire Line
	9750 1850 9900 1850
Wire Wire Line
	9750 1950 9900 1950
Wire Wire Line
	9750 2050 9900 2050
Wire Wire Line
	9750 2150 9900 2150
Wire Wire Line
	9750 2250 9900 2250
Wire Wire Line
	9750 4150 9900 4150
Wire Wire Line
	9750 4350 9900 4350
Wire Wire Line
	9750 4550 9900 4550
Entry Wire Line
	7700 5250 7800 5350
Entry Wire Line
	7700 5150 7800 5250
Entry Wire Line
	7700 5050 7800 5150
Entry Wire Line
	7700 4950 7800 5050
Entry Wire Line
	7700 4850 7800 4950
Entry Wire Line
	7700 4750 7800 4850
Entry Wire Line
	7700 4650 7800 4750
Entry Wire Line
	7700 4550 7800 4650
Entry Wire Line
	7700 4450 7800 4550
Entry Wire Line
	7700 4350 7800 4450
Entry Wire Line
	7700 4250 7800 4350
Entry Wire Line
	7700 4150 7800 4250
Entry Wire Line
	7700 4050 7800 4150
Entry Wire Line
	7700 3850 7800 3950
Entry Wire Line
	7700 3750 7800 3850
Entry Wire Line
	7700 3650 7800 3750
Entry Wire Line
	7700 3550 7800 3650
Entry Wire Line
	7700 3450 7800 3550
Entry Wire Line
	7700 3350 7800 3450
Entry Wire Line
	7700 3250 7800 3350
Entry Wire Line
	7700 3150 7800 3250
Entry Wire Line
	7700 2950 7800 3050
Entry Wire Line
	7700 2850 7800 2950
Entry Wire Line
	7700 2750 7800 2850
Entry Wire Line
	7700 2650 7800 2750
Entry Wire Line
	7700 2550 7800 2650
Entry Wire Line
	7700 2450 7800 2550
Entry Wire Line
	7700 2350 7800 2450
Entry Wire Line
	7700 2250 7800 2350
Entry Wire Line
	7700 2150 7800 2250
Entry Wire Line
	7700 2050 7800 2150
Entry Wire Line
	7700 1950 7800 2050
Entry Wire Line
	7700 1850 7800 1950
Entry Wire Line
	7700 1750 7800 1850
Entry Wire Line
	7700 1650 7800 1750
Entry Wire Line
	7700 1550 7800 1650
Entry Wire Line
	7700 1450 7800 1550
Entry Wire Line
	10000 1450 9900 1550
Entry Wire Line
	10000 1550 9900 1650
Entry Wire Line
	10000 1650 9900 1750
Entry Wire Line
	10000 1750 9900 1850
Entry Wire Line
	10000 1850 9900 1950
Entry Wire Line
	10000 1950 9900 2050
Entry Wire Line
	10000 2050 9900 2150
Entry Wire Line
	10000 2150 9900 2250
Entry Wire Line
	10000 2350 9900 2450
Entry Wire Line
	10000 2450 9900 2550
Entry Wire Line
	10000 2550 9900 2650
Entry Wire Line
	10000 2650 9900 2750
Entry Wire Line
	10000 2750 9900 2850
Entry Wire Line
	10000 2850 9900 2950
Entry Wire Line
	10000 2950 9900 3050
Entry Wire Line
	10000 3050 9900 3150
Entry Wire Line
	10000 3850 9900 3950
Entry Wire Line
	10000 3950 9900 4050
Entry Wire Line
	10000 4050 9900 4150
Entry Wire Line
	10000 4150 9900 4250
Entry Wire Line
	10000 4250 9900 4350
Entry Wire Line
	10000 4350 9900 4450
Entry Wire Line
	10000 4450 9900 4550
Wire Bus Line
	10000 950  7700 950 
Wire Notes Line
	6800 600  6800 7650
Text GLabel 10200 4900 2    50   BiDi ~ 0
BUS_FRDM
Text GLabel 5100 4850 2    50   BiDi ~ 0
BUS_FRDM
$Sheet
S 4650 5850 900  850 
U 5EADD493
F0 "sdcard" 50
F1 "sdcard.sch" 50
$EndSheet
Text GLabel 2500 3200 0    50   Output ~ 0
CS_SD
$Comp
L driveshield-rescue:JUMPER3-devices JP1
U 1 1 5ED20B49
P 1350 4650
F 0 "JP1" V 1441 4752 50  0000 L CNN
F 1 "CONN_01X03" V 1350 4752 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" V 1259 4752 50  0001 L CNN
F 3 "" H 1350 4650 50  0000 C CNN
	1    1350 4650
	0    1    1    0   
$EndComp
$Comp
L driveshield-rescue:+3.3V-power-supply #PWR01
U 1 1 5ED49D3C
P 1350 4400
F 0 "#PWR01" H 1350 4250 50  0001 C CNN
F 1 "+3.3V" H 1365 4573 50  0000 C CNN
F 2 "" H 1350 4400 50  0000 C CNN
F 3 "" H 1350 4400 50  0000 C CNN
	1    1350 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 4650 1150 4650
Text GLabel 2550 2700 0    50   Output ~ 0
CS_DISPLAY
$Comp
L driveshield-rescue:GND-power-supply #PWR05
U 1 1 5EBCA69B
P 9800 5300
F 0 "#PWR05" H 9800 5050 50  0001 C CNN
F 1 "GND" H 9805 5127 50  0000 C CNN
F 2 "" H 9800 5300 50  0000 C CNN
F 3 "" H 9800 5300 50  0000 C CNN
	1    9800 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4950 9800 4950
Wire Wire Line
	9650 5050 9800 5050
Wire Wire Line
	9800 5050 9800 5150
Wire Wire Line
	9650 5150 9800 5150
Connection ~ 9800 5150
Wire Wire Line
	9800 5150 9800 5250
Wire Wire Line
	9650 5250 9800 5250
Connection ~ 9800 5250
Wire Wire Line
	9800 5250 9800 5300
Wire Wire Line
	9800 4950 9800 5050
Connection ~ 9800 5050
$Comp
L driveshield-rescue:FRDM-KL25Z_bot-FRDM-KL25Z J2
U 1 1 5EB045F3
P 9150 2450
AR Path="/5EB045F3" Ref="J2"  Part="1" 
AR Path="/5F6A4F39/5EB045F3" Ref="J?"  Part="1" 
F 0 "J2" H 8850 3617 50  0000 C CNN
F 1 "FRDM-KL25Z" H 8850 3526 50  0000 C CNN
F 2 "FRDM_KL25Z:FRDM-KL25Z-header-bot" H 9150 2450 50  0001 L BNN
F 3 "" H 9150 2450 50  0001 C CNN
F 4 "1" H 9150 2450 50  0001 C CNN "DNP"
	1    9150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4800 1600 4900
Wire Wire Line
	4950 3900 4950 3800
Wire Wire Line
	4950 3800 5050 3800
Wire Wire Line
	4650 1700 4800 1700
Wire Wire Line
	4650 1800 4800 1800
$Comp
L driveshield-rescue:R_0603-devices R7
U 1 1 5F16BDE6
P 1150 5000
F 0 "R7" H 1209 5046 50  0000 L CNN
F 1 "RES 10k 10% 0603" H 1209 4955 50  0000 L CNN
F 2 "resistors:R_0603" H 1150 4850 50  0001 C CNN
F 3 "" H 1150 5000 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 1150 5000 50  0001 C CNN "manf#"
F 5 "1" H 1150 5000 50  0001 C CNN "check"
	1    1150 5000
	-1   0    0    -1  
$EndComp
$Comp
L driveshield-rescue:GND-power-supply #PWR0106
U 1 1 5F16C410
P 1150 5250
F 0 "#PWR0106" H 1150 5000 50  0001 C CNN
F 1 "GND" H 1155 5077 50  0000 C CNN
F 2 "" H 1150 5250 50  0000 C CNN
F 3 "" H 1150 5250 50  0000 C CNN
	1    1150 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5250 1150 5100
Wire Wire Line
	1150 4900 1150 4650
Connection ~ 1150 4650
Wire Wire Line
	1150 4650 1050 4650
Text GLabel 2500 1500 0    50   Input ~ 0
FEEDBACK_A
Text GLabel 2500 5650 0    50   Input ~ 0
FEEDBACK_B
Text GLabel 2250 4900 0    50   Input ~ 0
MOTOR_A_SENS
Text GLabel 2250 5000 0    50   Input ~ 0
MOTOR_B_SENS
Text GLabel 2550 2900 0    50   Output ~ 0
SW2_EN
Wire Wire Line
	1600 4900 1350 4900
$Comp
L driveshield-rescue:Jumper_NC_Small-devices JP7
U 1 1 5FF9C891
P 2450 4900
F 0 "JP7" H 2350 4950 50  0000 C CNN
F 1 "Jumper_solder" H 2460 4840 50  0001 C CNN
F 2 "wire_pads:SolderJumper_2mm" H 2450 4900 50  0001 C CNN
F 3 "" H 2450 4900 50  0000 C CNN
F 4 "1" H 2450 4900 50  0001 C CNN "DNP"
	1    2450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5000 2350 5000
Wire Wire Line
	2250 4900 2350 4900
$Comp
L driveshield-rescue:Jumper_NC_Small-devices JP8
U 1 1 5FFD5AA2
P 2450 5000
F 0 "JP8" H 2350 5050 50  0000 C CNN
F 1 "Jumper_solder" H 2460 4940 50  0001 C CNN
F 2 "wire_pads:SolderJumper_2mm" H 2450 5000 50  0001 C CNN
F 3 "" H 2450 5000 50  0000 C CNN
F 4 "1" H 2450 5000 50  0001 C CNN "DNP"
	1    2450 5000
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+6V-power-supply #PWR0101
U 1 1 5F01C449
P 5450 4200
F 0 "#PWR0101" H 5450 4050 50  0001 C CNN
F 1 "+6V" H 5465 4373 50  0000 C CNN
F 2 "" H 5450 4200 50  0000 C CNN
F 3 "" H 5450 4200 50  0000 C CNN
	1    5450 4200
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:FRDM-KL25Z_top-FRDM-KL25Z J1
U 1 1 5EA586FA
P 4050 2400
F 0 "J1" H 3750 3567 50  0000 C CNN
F 1 "FRDM-KL25Z" H 3750 3476 50  0000 C CNN
F 2 "FRDM_KL25Z:FRDM-KL25Z-header-top" H 4050 2400 50  0001 L BNN
F 3 "" H 4050 2400 50  0001 C CNN
F 4 "1" H 4050 2400 50  0001 C CNN "DNP"
	1    4050 2400
	1    0    0    -1  
$EndComp
NoConn ~ 4550 3700
Text GLabel 4950 2100 2    50   Output ~ 0
DSP_CD
Entry Wire Line
	2600 5200 2700 5300
Wire Wire Line
	2850 5300 2700 5300
Wire Wire Line
	2550 5200 2550 5650
Wire Wire Line
	2550 5650 2500 5650
Wire Wire Line
	2500 5100 2500 5550
Wire Wire Line
	2500 5550 2450 5550
Text Notes 1350 5250 0    50   ~ 0
Current sense optional to\npreserve Pins for expansions
$Comp
L driveshield-rescue:PWR_FLAG-power-supply #FLG0103
U 1 1 5F38AA27
P 5200 4000
F 0 "#FLG0103" H 5200 4095 50  0001 C CNN
F 1 "PWR_FLAG" H 5200 4223 50  0000 C CNN
F 2 "" H 5200 4000 50  0000 C CNN
F 3 "" H 5200 4000 50  0000 C CNN
	1    5200 4000
	-1   0    0    1   
$EndComp
Connection ~ 5200 4000
Wire Wire Line
	5200 4000 5300 4000
Wire Wire Line
	4650 3100 4950 3100
Wire Wire Line
	2550 3000 2850 3000
Wire Wire Line
	2550 3900 2850 3900
Wire Wire Line
	4650 2200 5600 2200
Wire Wire Line
	4650 4400 5000 4400
Wire Wire Line
	4650 4200 5450 4200
Wire Wire Line
	4650 4000 5200 4000
Wire Wire Line
	4650 3900 4950 3900
Wire Wire Line
	4650 3000 4950 3000
Wire Wire Line
	4650 2900 4950 2900
Wire Wire Line
	4650 2800 4950 2800
Wire Wire Line
	4650 2700 4950 2700
Wire Wire Line
	4650 2600 4950 2600
Wire Wire Line
	4650 2500 4950 2500
Wire Wire Line
	4650 2400 4950 2400
Wire Wire Line
	4650 2100 4950 2100
Wire Wire Line
	4650 1600 5600 1600
Wire Wire Line
	4650 1500 5600 1500
Wire Wire Line
	2500 1500 2850 1500
Wire Wire Line
	2500 1600 2850 1600
Wire Wire Line
	2500 1700 2850 1700
Wire Wire Line
	2500 1800 2850 1800
Wire Wire Line
	2500 1900 2850 1900
Wire Wire Line
	2500 2300 2850 2300
Wire Wire Line
	2500 2400 2850 2400
Wire Wire Line
	2550 2700 2850 2700
Wire Wire Line
	2550 2800 2850 2800
Wire Wire Line
	2550 2900 2850 2900
Wire Wire Line
	2500 3200 2850 3200
Wire Wire Line
	2500 3300 2850 3300
Wire Wire Line
	2500 3400 2850 3400
Wire Wire Line
	2500 3500 2850 3500
Wire Wire Line
	2550 3700 2850 3700
Wire Wire Line
	2550 3800 2850 3800
Wire Wire Line
	1850 4100 2850 4100
Wire Wire Line
	1950 4200 2850 4200
Wire Wire Line
	2550 4300 2850 4300
Wire Wire Line
	2550 4400 2850 4400
Wire Wire Line
	2550 4500 2850 4500
Wire Wire Line
	2550 4600 2850 4600
Wire Wire Line
	2550 4700 2850 4700
Wire Wire Line
	1600 4800 2850 4800
Wire Wire Line
	2550 4900 2850 4900
Wire Wire Line
	2550 5000 2850 5000
Wire Wire Line
	2500 5100 2850 5100
Wire Wire Line
	2550 5200 2850 5200
Wire Bus Line
	4900 900  4900 4850
Wire Bus Line
	10000 950  10000 4900
Wire Bus Line
	2600 900  2600 5200
Wire Bus Line
	7700 950  7700 5250
$EndSCHEMATC
