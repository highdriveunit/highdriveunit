EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title "nxpboard"
Date "2020-08-08"
Rev "1.1"
Comp "THD"
Comment1 "Jonas Wühr"
Comment2 "Lucas Bartosch"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L driveshield-rescue:CONN_01X05-mechanical-connectors P7
U 1 1 5F67B9DA
P 3400 1500
F 0 "P7" H 3478 1586 50  0000 L CNN
F 1 "CONN_01X05" H 3478 1495 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 3350 1000 50  0000 L CNN
F 3 "" H 3400 1500 50  0000 C CNN
	1    3400 1500
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C49
U 1 1 5F680EC9
P 2600 1750
F 0 "C49" H 2692 1796 50  0000 L CNN
F 1 "CAP CER 100nF 6V 0603" H 2692 1705 50  0000 L CNN
F 2 "capacitors:C_0603" H 2600 1600 50  0001 C CNN
F 3 "" H 2600 1750 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 2600 1750 50  0001 C CNN "manf#"
F 5 "1" H 2600 1750 50  0001 C CNN "check"
	1    2600 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1700 3850 1700
Wire Wire Line
	3850 1700 3850 1900
Wire Wire Line
	3850 1900 3100 1900
Wire Wire Line
	3100 1900 3100 1700
Wire Wire Line
	3100 1700 3200 1700
Wire Wire Line
	3100 1900 2600 1900
Wire Wire Line
	2600 1900 2600 1850
Connection ~ 3100 1900
$Comp
L driveshield-rescue:GND-power-supply #PWR080
U 1 1 5F6835A9
P 3100 1950
F 0 "#PWR080" H 3100 1700 50  0001 C CNN
F 1 "GND" H 3105 1777 50  0000 C CNN
F 2 "" H 3100 1950 50  0000 C CNN
F 3 "" H 3100 1950 50  0000 C CNN
	1    3100 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1950 3100 1900
Wire Wire Line
	3200 1600 2600 1600
Wire Wire Line
	2600 1600 2600 1650
Wire Wire Line
	2600 1600 2350 1600
Wire Wire Line
	2350 1600 2350 1900
Wire Wire Line
	2350 1900 2150 1900
Wire Wire Line
	2150 1900 2150 1800
Connection ~ 2600 1600
$Comp
L driveshield-rescue:+3V3-power-supply #PWR076
U 1 1 5F683FD0
P 2150 1800
F 0 "#PWR076" H 2150 1650 50  0001 C CNN
F 1 "+3V3" H 2165 1973 50  0000 C CNN
F 2 "" H 2150 1800 50  0000 C CNN
F 3 "" H 2150 1800 50  0000 C CNN
	1    2150 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1300 3850 1300
Wire Wire Line
	3850 1300 3850 1100
Wire Wire Line
	3850 1100 2950 1100
Wire Wire Line
	2950 1100 2950 1300
Wire Wire Line
	2950 1300 3200 1300
Wire Wire Line
	2950 1300 2100 1300
Connection ~ 2950 1300
Wire Wire Line
	3200 1400 2100 1400
Text GLabel 2100 1300 0    50   Output ~ 0
CAM0_AI
Text GLabel 2100 1400 0    50   Input ~ 0
CAM_SYNC
Text GLabel 2100 1500 0    50   Input ~ 0
CAM_CLK
$Comp
L driveshield-rescue:CONN_01X05-mechanical-connectors P12
U 1 1 5F688E97
P 4300 3050
F 0 "P12" H 4378 3136 50  0000 L CNN
F 1 "CONN_01X05" H 4378 3045 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 4378 2954 50  0000 L CNN
F 3 "" H 4300 3050 50  0000 C CNN
	1    4300 3050
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:CONN_01X05-mechanical-connectors P8
U 1 1 5F688E9D
P 3400 3050
F 0 "P8" H 3478 3136 50  0000 L CNN
F 1 "CONN_01X05" H 3478 3045 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 3550 2600 50  0000 L CNN
F 3 "" H 3400 3050 50  0000 C CNN
	1    3400 3050
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:C_0603-devices C50
U 1 1 5F688EA3
P 2600 3300
F 0 "C50" H 2692 3346 50  0000 L CNN
F 1 "CAP CER 100nF 6V 0603" H 2692 3255 50  0000 L CNN
F 2 "capacitors:C_0603" H 2600 3150 50  0001 C CNN
F 3 "" H 2600 3300 50  0000 C CNN
F 4 "C0603C104M4RACTU" H 2600 3300 50  0001 C CNN "manf#"
F 5 "1" H 2600 3300 50  0001 C CNN "check"
	1    2600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3250 3850 3250
Wire Wire Line
	3850 3250 3850 3450
Wire Wire Line
	3850 3450 3100 3450
Wire Wire Line
	3100 3450 3100 3250
Wire Wire Line
	3100 3250 3200 3250
Wire Wire Line
	3100 3450 2600 3450
Wire Wire Line
	2600 3450 2600 3400
Connection ~ 3100 3450
$Comp
L driveshield-rescue:GND-power-supply #PWR081
U 1 1 5F688EB1
P 3100 3500
F 0 "#PWR081" H 3100 3250 50  0001 C CNN
F 1 "GND" H 3105 3327 50  0000 C CNN
F 2 "" H 3100 3500 50  0000 C CNN
F 3 "" H 3100 3500 50  0000 C CNN
	1    3100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3500 3100 3450
Wire Wire Line
	3200 3150 2600 3150
Wire Wire Line
	2600 3150 2600 3200
Wire Wire Line
	2600 3150 2350 3150
Wire Wire Line
	2350 3150 2350 3450
Wire Wire Line
	2350 3450 2150 3450
Wire Wire Line
	2150 3450 2150 3350
Connection ~ 2600 3150
$Comp
L driveshield-rescue:+3V3-power-supply #PWR077
U 1 1 5F688EBF
P 2150 3350
F 0 "#PWR077" H 2150 3200 50  0001 C CNN
F 1 "+3V3" H 2165 3523 50  0000 C CNN
F 2 "" H 2150 3350 50  0000 C CNN
F 3 "" H 2150 3350 50  0000 C CNN
	1    2150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2850 3850 2850
Wire Wire Line
	3850 2850 3850 2650
Wire Wire Line
	3850 2650 2950 2650
Wire Wire Line
	2950 2650 2950 2850
Wire Wire Line
	2950 2850 3200 2850
Wire Wire Line
	2950 2850 2100 2850
Connection ~ 2950 2850
Wire Wire Line
	3200 2950 2100 2950
Wire Wire Line
	3200 3050 3100 3050
Text GLabel 2100 2850 0    50   Output ~ 0
CAM1_AI
Text GLabel 2100 2950 0    50   Input ~ 0
CAM_SYNC
Text GLabel 2100 3050 0    50   Input ~ 0
CAM_CLK
$Comp
L driveshield-rescue:CONN_01X03-mechanical-connectors P10
U 1 1 5F68A968
P 3750 5000
F 0 "P10" H 3828 5086 50  0000 L CNN
F 1 "CONN_01X03" H 3828 4995 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3828 4904 50  0000 L CNN
F 3 "" H 3750 5000 50  0000 C CNN
	1    3750 5000
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR079
U 1 1 5F68E1D6
P 3000 4550
F 0 "#PWR079" H 3000 4400 50  0001 C CNN
F 1 "+3V3" H 3015 4723 50  0000 C CNN
F 2 "" H 3000 4550 50  0000 C CNN
F 3 "" H 3000 4550 50  0000 C CNN
	1    3000 4550
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R70
U 1 1 5F68EBC6
P 3000 4800
F 0 "R70" H 3059 4846 50  0000 L CNN
F 1 "RES 10k 10% 0603" H 3059 4755 50  0000 L CNN
F 2 "resistors:R_0603" H 3000 4650 50  0001 C CNN
F 3 "" H 3000 4800 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 3000 4800 50  0001 C CNN "manf#"
F 5 "1" H 3000 4800 50  0001 C CNN "check"
	1    3000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 5000 3000 5000
Wire Wire Line
	3000 5000 3000 4900
Wire Wire Line
	3000 4700 3000 4650
Wire Wire Line
	3550 4900 3350 4900
Wire Wire Line
	3350 4900 3350 4650
Wire Wire Line
	3350 4650 3000 4650
Connection ~ 3000 4650
Wire Wire Line
	3000 4650 3000 4550
Wire Wire Line
	3550 5100 3350 5100
Wire Wire Line
	3350 5100 3350 5300
$Comp
L driveshield-rescue:GND-power-supply #PWR083
U 1 1 5F699085
P 3350 5300
F 0 "#PWR083" H 3350 5050 50  0001 C CNN
F 1 "GND" H 3355 5127 50  0000 C CNN
F 2 "" H 3350 5300 50  0000 C CNN
F 3 "" H 3350 5300 50  0000 C CNN
	1    3350 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5000 2550 5000
Connection ~ 3000 5000
Text GLabel 2550 5000 0    50   Output ~ 0
SENSOR_0
$Comp
L driveshield-rescue:CONN_01X03-mechanical-connectors P9
U 1 1 5F69B46A
P 3700 6650
F 0 "P9" H 3778 6736 50  0000 L CNN
F 1 "CONN_01X03" H 3778 6645 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3778 6554 50  0000 L CNN
F 3 "" H 3700 6650 50  0000 C CNN
	1    3700 6650
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:+3V3-power-supply #PWR078
U 1 1 5F69B470
P 2950 6200
F 0 "#PWR078" H 2950 6050 50  0001 C CNN
F 1 "+3V3" H 2965 6373 50  0000 C CNN
F 2 "" H 2950 6200 50  0000 C CNN
F 3 "" H 2950 6200 50  0000 C CNN
	1    2950 6200
	1    0    0    -1  
$EndComp
$Comp
L driveshield-rescue:R_0603-devices R69
U 1 1 5F69B476
P 2950 6450
F 0 "R69" H 3009 6496 50  0000 L CNN
F 1 "RES 10k 10% 0603" H 3009 6405 50  0000 L CNN
F 2 "resistors:R_0603" H 2950 6300 50  0001 C CNN
F 3 "" H 2950 6450 50  0000 C CNN
F 4 "RC0603FR-0710KL" H 2950 6450 50  0001 C CNN "manf#"
F 5 "1" H 2950 6450 50  0001 C CNN "check"
	1    2950 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6650 2950 6650
Wire Wire Line
	2950 6650 2950 6550
Wire Wire Line
	2950 6350 2950 6300
Wire Wire Line
	3500 6550 3300 6550
Wire Wire Line
	3300 6550 3300 6300
Wire Wire Line
	3300 6300 2950 6300
Connection ~ 2950 6300
Wire Wire Line
	2950 6300 2950 6200
Wire Wire Line
	3500 6750 3300 6750
Wire Wire Line
	3300 6750 3300 6950
$Comp
L driveshield-rescue:GND-power-supply #PWR082
U 1 1 5F69B486
P 3300 6950
F 0 "#PWR082" H 3300 6700 50  0001 C CNN
F 1 "GND" H 3305 6777 50  0000 C CNN
F 2 "" H 3300 6950 50  0000 C CNN
F 3 "" H 3300 6950 50  0000 C CNN
	1    3300 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 6650 2500 6650
Connection ~ 2950 6650
Text GLabel 2500 6650 0    50   Output ~ 0
SENSOR_1
Wire Wire Line
	4100 3150 4100 3250
Connection ~ 4100 3250
Connection ~ 4100 1700
Wire Wire Line
	4100 1600 4100 1700
Connection ~ 4100 1600
$Comp
L driveshield-rescue:CONN_01X05-mechanical-connectors P11
U 1 1 5F67A100
P 4300 1500
F 0 "P11" H 4378 1586 50  0000 L CNN
F 1 "CONN_01X05" H 4378 1495 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 4378 1404 50  0000 L CNN
F 3 "" H 4300 1500 50  0000 C CNN
	1    4300 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1500 3150 1150
Wire Wire Line
	3150 1150 3700 1150
Wire Wire Line
	3700 1150 3700 1450
Wire Wire Line
	3700 1450 4050 1450
Wire Wire Line
	4050 1450 4050 1500
Wire Wire Line
	4050 1500 4100 1500
Wire Wire Line
	2100 1500 3150 1500
Connection ~ 3150 1500
Wire Wire Line
	3150 1500 3200 1500
Wire Wire Line
	4100 1400 4000 1400
Wire Wire Line
	4000 1400 4000 1600
Wire Wire Line
	4000 1600 4100 1600
Wire Wire Line
	4100 3050 4000 3050
Wire Wire Line
	4000 3050 4000 3000
Wire Wire Line
	4000 3000 3600 3000
Wire Wire Line
	3600 3000 3600 2750
Wire Wire Line
	3600 2750 3100 2750
Wire Wire Line
	3100 2750 3100 3050
Connection ~ 3100 3050
Wire Wire Line
	3100 3050 2100 3050
Wire Wire Line
	4100 2950 4050 2950
Wire Wire Line
	4050 2950 4050 3150
Wire Wire Line
	4050 3150 4100 3150
Connection ~ 4100 3150
Text Notes 4450 1200 0    50   ~ 0
Pull pin 2 and 4 from header to get easily\nhookable measurement points of camera \nclock and output for oscilloscope
Text Notes 4450 2850 0    50   ~ 0
Pull pin 2 and 4 from header to get easily\nhookable measurement points of camera \nclock and output for oscilloscope
$EndSCHEMATC
